#ifndef CLIPEDITORWIDGET_H
#define CLIPEDITORWIDGET_H

#include <QWidget>

#include "Widgets/MusicTreeView.h"
#include "Widgets/MixerWidget.h"
#include "Widgets/TimelineWidget.h"

namespace Ui {
class ClipEditorWidget;
}

class ClipEditorWidget : public QWidget
{
	Q_OBJECT

public:
	explicit ClipEditorWidget(QWidget *parent = Q_NULLPTR);
	~ClipEditorWidget();

	void setClipEditor(ClipContainerPtr);

private:
	Ui::ClipEditorWidget *ui = Q_NULLPTR;

	MixerWidget *widgetMixer = Q_NULLPTR;
	TimelineWidget *widgetTimeline = Q_NULLPTR;
};

#endif // CLIPEDITORWIDGET_H
