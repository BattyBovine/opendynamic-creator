#ifndef CLIPGROUPEDITORWIDGET_H
#define CLIPGROUPEDITORWIDGET_H

#include <QScrollArea>

#include "Widgets/MusicTreeView.h"
#include "Widgets/MixerWidget.h"
#include "Widgets/TimelineWidget.h"

namespace Ui {
class ClipGroupEditorWidget;
}

class ClipGroupEditorWidget : public QScrollArea
{
	Q_OBJECT

public:
	explicit ClipGroupEditorWidget(QWidget *parent = Q_NULLPTR);
	~ClipGroupEditorWidget();

	void setClipGroupEditor(ClipContainerPtr);

private:
	Ui::ClipGroupEditorWidget *ui = Q_NULLPTR;

	TimelineWidget *twMasterTimeline = Q_NULLPTR;
};

#endif // CLIPGROUPEDITORWIDGET_H
