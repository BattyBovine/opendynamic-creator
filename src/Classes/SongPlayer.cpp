#include "SongPlayer.h"
#include "Classes/EventCommand.h"


SongPlayer::SongPlayer(ClipContainerPtr clip, QObject *parent) : QObject(parent)
{
	this->setClip(clip);
}

SongPlayer::~SongPlayer()
{
//	if(this->cpActiveSegment)
//		this->cpActiveSegment->deleteLater();
//	if(this->cpTransitionSegment)
//		this->cpTransitionSegment->deleteLater();
//	while(!this->queueSegments.isEmpty())
//		this->queueSegments.dequeue()->deleteLater();
}


SongPlayer::Error SongPlayer::setClip(ClipContainerPtr clip)
{
	if(clip) {
		QAudioDeviceInfo deviceinfo = QAudioDeviceInfo::defaultOutputDevice();
		QAudioFormat format;
		format.setSampleRate(signed(clip->sampleRate()));
		format.setChannelCount(clip->channelCount());
		format.setSampleSize(clip->bytesPerSample() * 8);
		format.setCodec("audio/pcm");
		format.setByteOrder(QAudioFormat::LittleEndian);
		format.setSampleType(QAudioFormat::SignedInt);
		if(!deviceinfo.isFormatSupported(format))
			format = deviceinfo.nearestFormat(format);
		this->aoTestOutput.reset(new QAudioOutput(deviceinfo, format));
//		this->aoTestOutput->setBufferSize(quint32(4) * clip->bytesPerSample() * clip->channelCount());

		this->uuidActiveClip = clip->uuid();
		while(clip->clipParent())
			clip = clip->clipParent();
		return this->searchItemHierarchy(clip);
	}
	return Error::SP_INVALID_TRACK;
}

SongPlayer::Error SongPlayer::searchItemHierarchy(ClipContainerPtr clip)
{
	Error error = Error::SP_OK;
	this->hashClips[clip->uuid()] = clip;
	switch(clip->type())
	{
	case ClipContainerType::AUDIO:
		this->hashGroupMap.insert(clip->uuid(), clip->uuid());
		break;
	case ClipContainerType::GROUP: {
		ClipGroupContainerPtr clipgroup = std::static_pointer_cast<ClipGroupContainer>(clip);
		const QList<ClipContainerPtr> &childclips = clipgroup->childClips();
		foreach(ClipContainerPtr child, childclips) {
			this->hashGroupMap.insert(clip->uuid(), child->uuid());
			this->searchItemHierarchy(child);
		}
	}	break;
	default:
		break;
	}
	return error;
}
//ClipPlayer *SongPlayer::createClipPlayer(const QUuid &clipgroup)
//{
//	ClipPlayer *cplay = new ClipPlayer(this->hashClips[clipgroup], this);
//	QList<QUuid> groupclips = this->hashGroupMap.values(clipgroup);
//	foreach(QUuid clip, groupclips) {
//		ClipContainerPtr cc = this->hashClips[clip];
//		switch(cc->type()) {
//		case ClipContainerType::GROUP:
//		{
//			ClipGroupContainerPtr cgc = std::static_pointer_cast<ClipGroupContainer>(cc);
//			const QList<ClipContainerPtr>& children = cgc->childClips();
//			for(ClipContainerPtr clip : children) {
//				if(clip->type()==ClipContainerType::AUDIO)
//					cplay->addClipData(std::static_pointer_cast<AudioClipContainer>(clip));
//			}
//		}	break;
//		case ClipContainerType::AUDIO:
//			cplay->addClipData(std::static_pointer_cast<AudioClipContainer>(cc));
//		default:
//			break;
//		}
//	}
//	return cplay;
//}



SongPlayer::Error SongPlayer::playSong(const QUuid &startclip)
{
	this->uuidActiveClip = startclip;
	return this->playSong();
}

SongPlayer::Error SongPlayer::playSong()
{
	ClipContainerPtr clip = this->hashClips[this->uuidActiveClip];
	if(clip) {
		clip->open(QIODevice::ReadOnly);
		this->aoTestOutput->start(clip.get());
		return Error::SP_OK;
	}
	return Error::SP_INVALID_ACTIVE_SEGMENT;
}

void SongPlayer::pauseSong()
{
	this->aoTestOutput->suspend();
}

void SongPlayer::stopSong()
{
	this->aoTestOutput->stop();
	this->hashClips[this->uuidActiveClip]->rewind();
}

//void SongPlayer::applyEvent(MusicEvent *me)
//{
//	qDebug() << QString("Event fired: %1").arg(me->uuidString());
//	EventCommandList commands = me->commands();
////	QList<QUuid> cliplist = this->hashGroupMap.values(uuid);
////	foreach(QUuid clipid, cliplist) {
//	foreach(EventCommandPtr command, commands)
//		command->applyEvent(this->hashClips[command->clip()]);
////	}
//}
