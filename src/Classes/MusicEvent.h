#ifndef MUSICEVENT_H
#define MUSICEVENT_H

#include <memory>

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QVector>
#include <QUuid>
#include <QBuffer>
#include <QAudio>

class EventCommand;
typedef std::shared_ptr<EventCommand> EventCommandPtr;
typedef QList<EventCommandPtr> EventCommandList;


class MusicEvent
{
public:
	MusicEvent() { this->uuidUnique=QUuid::createUuid(); }

	void setName(QString &n) { this->sName=n; }
	void setActive(bool a) { this->bActive=a; }
	void addCommand(EventCommandPtr e);

	QString name() const { return this->sName; }
	QUuid uuid() const { return this->uuidUnique; }
	QString uuidString() const { return this->uuidUnique.toString(); }
	bool active() const { return this->bActive; }
	const EventCommandList &commands() { return this->lCommands; }

private:
	QUuid uuidUnique;
	QString sName;
	bool bActive = true;
	EventCommandList lCommands;
};
typedef std::shared_ptr<MusicEvent> MusicEventPtr;

#endif // MUSICEVENT_H
