#ifndef CLIPCONTAINER_H
#define CLIPCONTAINER_H

#include <memory>
#include <vorbis/vorbisfile.h>

#include <QSettings>
#include <QUrl>
#include <QAudioOutput>
#include <QByteArray>
#include <QBuffer>
#include <QTimer>
#include <QPixmap>
#include <QPainter>
#include <QUuid>

#include "PreferencesDialog.h"
#include "StaticMusicEvent.h"


enum class ClipContainerType : quint8
{
	BASE,
	AUDIO,
	GROUP
};

struct LoopSegment
{
	LoopSegment(){}
	LoopSegment(Beat pos, Beat len) { this->position=pos; this->length=len; }
	Beat position = Beat(0);
	Beat length = Beat(0);
};

typedef std::shared_ptr<class ClipContainer> ClipContainerPtr;
typedef std::shared_ptr<class AudioClipContainer> AudioClipContainerPtr;
typedef std::shared_ptr<class ClipGroupContainer> ClipGroupContainerPtr;


class ClipContainer : public QIODevice
{
	Q_OBJECT

public:
	enum class Error : quint8 {
		OK,
		FILE_READ_ERROR,
		FORMAT_UNRECOGNIZED
	};

	ClipContainer(ClipContainerPtr parent=Q_NULLPTR);
	ClipContainer(const ClipContainer&);
	ClipContainer &operator=(const ClipContainer&);
	virtual void copy(const ClipContainer&);
	virtual ClipContainerType type() { return ClipContainerType::BASE; }

	void setSampleRate(quint32 s) { this->iSampleRate=s; }
	void setName(QString n) { this->sName=n; }
	virtual void setTimeInfo(qreal t, quint8 b, quint8 u) {
		this->rTempo = t;
		this->iBeatsPerMeasure = b;
		this->iBeatUnit = u;
	}

	virtual void addChild(ClipContainerPtr){}
	virtual void removeChild(QUuid&){}

	virtual void setParentClip(ClipContainerPtr parent, ClipContainerPtr) { this->removeFromParent(); this->ccParent=parent; }
	void removeFromParent();
	ClipContainerPtr clipParent() { return this->ccParent; }

	QUuid &uuid() { return this->uuidUnique; }
	QString uuidString() { return this->uuidUnique.toString(); }
	virtual quint32 sampleRate() const { return this->iSampleRate; }
	virtual quint16 channelCount() const { return this->iChannelCount; }
	virtual quint16 bitrateLower() const { return this->iLowerBitrate; }
	virtual quint16 bitrateNominal() const { return this->iNominalBitrate; }
	virtual quint16 bitrateUpper() const { return this->iUpperBitrate; }
	virtual quint16 bitrateWindow() const { return this->iBitrateWindow; }
	virtual quint16 bytesPerSample() const { return this->iBytesPerSample; }
	QAudioOutput *createPlayer();

	void setVolume(qint16 v) { this->iVolume=v; emit(volumeChanged(this->volumedB())); emit(mixedVolumeChanged(this->mixedVolumedB())); }
	void setPositionBeats(Beat b=Beat()) { this->setPositionSeconds(b.toSeconds(this->rTempo,this->iBeatUnit)); }
	virtual void setPositionSeconds(qreal s) { this->rPositionSeconds=s; this->iPositionBytes=ClipContainer::secondsToBytes(s,this->iSampleRate,this->iBytesPerSample,this->iChannelCount); emit(secondsPositionChanged(s)); }

	virtual qint64 readData(char*, qint64) override { return 0; }
	virtual qint64 writeData(const char*, qint64) override { return 0; }
	virtual qint64 bytesAvailable() const override { return 0; }
	virtual QList<qint16> read16BitData(qint64) { return QList<qint16>(); }
	virtual void rewind() { this->iPositionBytes = 0; }

	virtual void setTimelineOffset(Beat b) { this->beatTimelineOffset=b; emit(timelineOffsetChanged(b)); }
	qreal length() const { return this->rLengthSeconds; }
	Beat beats() const { return this->beatLength; }

	QString name() const { return this->sName; }
	qreal tempo() const { return this->rTempo; }
	quint8 beatsPerMeasure() const { return this->iBeatsPerMeasure; }
	quint8 beatUnit() const { return this->iBeatUnit; }
	Beat timelineOffset() const { return this->beatTimelineOffset; }
	qint16 volumedB() const { return this->iVolume; }
	qint16 mixedVolumedB() const { return (this->iVolume + (this->ccParent ? this->ccParent->mixedVolumedB() : 0)); }
	qreal volumePercent() const { return QAudio::convertVolume(this->volumedB(), QAudio::DecibelVolumeScale, QAudio::LinearVolumeScale); }
	qreal mixedVolumePercent() const { return QAudio::convertVolume(this->mixedVolumedB(), QAudio::DecibelVolumeScale, QAudio::LinearVolumeScale); }
	qreal secondsElapsed() const { return this->rPositionSeconds; }

	void setLoopSegment(const LoopSegment &newloop) {
		this->loop = newloop;
		emit(loopSegmentChanged(this->loop));
	}
	void setLoopSegment(Beat pos, Beat length) {
		this->loop = LoopSegment(pos, length);
		emit(loopSegmentChanged(this->loop));
	}
	void addEvent(MusicEventPtr e, Beat pos) {
		StaticMusicEventPtr sme = std::make_shared<StaticMusicEvent>(StaticMusicEvent(e,pos));
		this->smelEvents.append(sme);
		std::sort(this->smelEvents.begin(), this->smelEvents.end(), [](StaticMusicEventPtr a, StaticMusicEventPtr b){return *a<*b;});
		emit(eventAdded(sme));
	}

	LoopSegment loopSegment() const { return this->loop; }
	StaticMusicEventList events() const { return this->smelEvents; }

	static qint64 secondsToBytes(qreal sec, quint32 samplerate, quint16 bytespersample, quint16 channelcount) { return qint64(round(samplerate * sec)) * bytespersample * channelcount; }
	static qreal bytesToSeconds(qint64 bytes, quint32 samplerate, quint16 bytespersample, quint16 channelcount) { return (bytes / qreal(bytespersample) / qreal(channelcount)) / qreal(samplerate); }
	static qint64 secondsToSamples(qreal sec, quint32 samplerate) { return qint64(round(samplerate * sec)); }
	static qreal samplesToSeconds(qint64 samples, quint32 samplerate) { return samples / qreal(samplerate); }

signals:
	void removedFromParent(QUuid&);
	void timelineOffsetChanged(Beat);
	void secondsPositionChanged(qreal);
	void volumeChanged(qint16);
	void mixedVolumeChanged(qint16);
	void loopSegmentChanged(LoopSegment);
	void eventAdded(StaticMusicEventPtr);
	void eventFired(MusicEventPtr);

protected:
	void setBeatLength(Beat b) { this->beatLength=b; }

	void advanceBytePosition(qint64);

	ClipContainerPtr ccParent = Q_NULLPTR;

	quint32 iSampleRate = 0;
	quint16 iChannelCount = 0;
	quint16 iLowerBitrate = 0;
	quint16 iNominalBitrate = 0;
	quint16 iUpperBitrate = 0;
	quint16 iBitrateWindow = 0;
	quint16 iBytesPerSample = 0;
	qreal rLengthSeconds = 0.0;
	qreal rPositionSeconds = 0.0;
	qint64 iPositionBytes = 0;
	Beat beatLength;

	QString sName;
	qreal rTempo = 0.0;
	quint8 iBeatsPerMeasure = 0;
	quint8 iBeatUnit = 0;
	qint16 iVolume = 0;
	Beat beatTimelineOffset;

	LoopSegment loop;
	StaticMusicEventList smelEvents;
	StaticMusicEventPtr smeNextEvent = Q_NULLPTR;

private:
	QUuid uuidUnique;
};

class AudioClipContainer : public ClipContainer
{
	Q_OBJECT

public:
	AudioClipContainer(ClipContainerPtr parent=Q_NULLPTR);
	virtual void copy(const ClipContainer&) override;
	virtual ClipContainerType type() override { return ClipContainerType::AUDIO; }

	virtual void setTimeInfo(qreal, quint8, quint8) override;
	virtual void setParentClip(ClipContainerPtr, ClipContainerPtr) override;

	virtual qint64 readData(char*, qint64) override;
	virtual qint64 bytesAvailable() const override { return this->baPCMData.length() - this->iPositionBytes; }
	virtual QList<qint16> read16BitData(qint64) override;

	Error loadAudioFile(QUrl);
	bool loadWav(QUrl);
	bool loadVorbis(QUrl);

	const char *rawData() { return this->baPCMData.data(); }
	quint64 rawDataLength() const { return quint64(this->baPCMData.size()); }
	QByteArray *pcmData() { return &this->baPCMData; }

protected:
	quint8 bLoop : 1;

private:
	virtual QByteArray pullBytesRecursively(qint64);

	QUrl urlFilePath;
	QByteArray baPCMData;
};

class ClipGroupContainer : public ClipContainer
{
	Q_OBJECT

public:
	virtual void copy(const ClipContainer&) override;
	virtual ClipContainerType type() override { return ClipContainerType::GROUP; }

	virtual void setTimelineOffset(Beat) override {}

	virtual void setPositionSeconds(qreal) override;

	virtual qint64 readData(char*, qint64) override;
	virtual qint64 bytesAvailable() const override;
	virtual QList<qint16> read16BitData(qint64) override;
	virtual void rewind() override;

	virtual void addChild(ClipContainerPtr) override;
	virtual void removeChild(QUuid&) override;

	Error loadChildClipInfo(const ClipContainer&);

	const QList<ClipContainerPtr>& childClips() const { return this->lChildClips; }
	quint32 childCount() const { return quint32(this->lChildClips.count()); }

public slots:
	void recalculateTimelineBounds();

private:
	QList<ClipContainerPtr> lChildClips;
};

#endif // CLIPCONTAINER_H
