#include "MusicEvent.h"
#include "ClipContainer.h"
#include "EventCommand.h"

void MusicEvent::addCommand(EventCommandPtr e)
{
	e->setParent(this);
	this->lCommands.append(e);
}
