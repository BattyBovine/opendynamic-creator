#include "ClipPlayer.h"
#include "SongPlayer.h"
#include "ClipContainer.h"



ClipPlayer::ClipPlayer(ClipContainerPtr clip, QObject *parent) : QObject(parent)
{
	this->uuidMainClip = clip->uuid();
	this->hashClips[this->uuidMainClip] = clip;
	connect(clip.get(), SIGNAL(mixedVolumeChanged(qint16)), this, SLOT(updateVolume()));
	connect(&this->timerPlayerMarkerPosition, SIGNAL(timeout()), this, SLOT(setClipPositions()));
}

ClipPlayer::~ClipPlayer()
{
	foreach(QAudioOutput *player, this->hashAudioPlayers)
		player->deleteLater();
	foreach(QBuffer *buffer, this->hashDataBuffers)
		buffer->deleteLater();
}



bool ClipPlayer::addClipData(ClipContainerPtr cc)
{
	const QUuid &uuid = cc->uuid();
	this->hashClips[uuid] = cc;
	QAudioOutput *player = cc->createPlayer();
	if(!player)
		return false;
	this->hashAudioPlayers[uuid] = player;
	connect(player, SIGNAL(stateChanged(QAudio::State)), this, SLOT(playerState(QAudio::State)));
	connect(cc.get(), SIGNAL(mixedVolumeChanged(qint16)), this, SLOT(updateVolume()));
	switch(cc->type()) {
	case ClipContainerType::AUDIO:
	{
		AudioClipContainerPtr acc = std::static_pointer_cast<AudioClipContainer>(cc);
		QByteArray *ba = acc->pcmData();
		QBuffer *buffer = new QBuffer(ba);
		buffer->open(QIODevice::ReadOnly);
		this->hashDataBuffers[uuid] = buffer;
	}	break;
	case ClipContainerType::GROUP:
	{
		ClipGroupContainerPtr cgc = std::static_pointer_cast<ClipGroupContainer>(cc);
		const QList<ClipContainerPtr> children = cgc->childClips();
		for(ClipContainerPtr clip : children)
			this->addClipData(clip);
	}
	default:
		break;
	}
	return true;
}

void ClipPlayer::playerState(QAudio::State s)
{
//	QAudioOutput *sender = (QAudioOutput*)QObject::sender();
	switch(s) {
//	case QAudio::ActiveState:
//		this->bIsPlaying = true;
//		break;
	case QAudio::StoppedState:
//		if(sender->error()!=QAudio::NoError)
//			qDebug() << sender->error();
		break;
	case QAudio::IdleState:
		emit(finished());
		break;
	default:
		break;
	}
}



void ClipPlayer::play(const SongPlayer *sp)
{
	this->timerPlayerMarkerPosition.start(1);
	this->startEventThread();
	connect(this->mewEvents, SIGNAL(musicEvent(MusicEvent*)), sp, SLOT(applyEvent(MusicEvent*)));
	BufferHash::Iterator bufferiter = this->hashDataBuffers.begin();
	foreach(QAudioOutput *player, this->hashAudioPlayers) {
		player->start(*bufferiter);
		bufferiter++;
	}
}

void ClipPlayer::stop()
{
	this->stopEventThread();
	foreach(QAudioOutput *player, this->hashAudioPlayers)
		player->stop();
	this->timerPlayerMarkerPosition.stop();
}

void ClipPlayer::updateVolume()
{
	ClipContainer *cc = static_cast<ClipContainer*>(QObject::sender());
	QUuid uuid = cc->uuid();
	if(this->hashAudioPlayers.contains(uuid)) {
		this->hashAudioPlayers[uuid]->setVolume(this->hashClips[uuid]->mixedVolumePercent());
	} else {
		if(this->uuidMainClip!=uuid)
			return;
		QList<QUuid> players = this->hashAudioPlayers.keys();
		foreach(QUuid player, players)
			this->hashAudioPlayers[player]->setVolume(this->hashClips[player]->mixedVolumePercent());
	}
}



void ClipPlayer::startEventThread()
{
	if(!this->mewEvents) {
		Beat smallestoffset(INT_MAX);
		const QList<QUuid> &buffers = this->hashDataBuffers.keys();
		QUuid clipwithsmallestoffset;
		foreach(const QUuid clipid, buffers) {
			Beat clipoffset = this->hashClips[clipid]->timelineOffset();
			if(clipoffset < smallestoffset) {
				smallestoffset = clipoffset;
				clipwithsmallestoffset = clipid;
			}
		}
		// TODO: Make this work with clip groups as well, since they should also have event flags despite containing no audio
		if(this->hashClips[this->uuidMainClip]->type()==ClipContainerType::AUDIO) {
			this->mewEvents = new MusicEventWorker(this->hashClips[this->uuidMainClip], this->hashDataBuffers[clipwithsmallestoffset]);
			connect(this->mewEvents, SIGNAL(musicEvent(MusicEvent*)), this, SLOT(setNextEvent()));
			connect(this->mewEvents, SIGNAL(finished()), this, SLOT(stopEventThread()));
			this->mewEvents->start(QThread::LowestPriority);
		}
	}
	this->smeNextEvent = this->hashClips[this->uuidMainClip]->events().cbegin();
	this->setNextEvent();
}
void ClipPlayer::setNextEvent()
{
	if(this->smeNextEvent==this->hashClips[this->uuidMainClip]->events().cend())
		return;
	this->mewEvents->setNextEvent((*this->smeNextEvent).get());
	this->smeNextEvent++;
}
void ClipPlayer::stopEventThread()
{
	if(this->mewEvents) {
		if(this->mewEvents->isRunning())
			this->mewEvents->stop();
		this->mewEvents->deleteLater();
		this->mewEvents = Q_NULLPTR;
	}
}



void ClipPlayer::setClipPositions()
{
	const QList<QUuid> &playingclips = this->hashDataBuffers.keys();
	foreach(const QUuid clipid, playingclips) {
		ClipContainerPtr cc = this->hashClips[clipid];
		cc->setPositionSeconds(ClipContainer::bytesToSeconds(unsigned(this->hashDataBuffers[clipid]->pos()),
															 cc->sampleRate(),
															 cc->bytesPerSample(),
															 cc->channelCount()));
	}
}
