#include "EventCommand.h"
#include "StaticMusicEvent.h"
#include "ClipContainer.h"


QUuid JumpToMarkerCommand::applyEvent(ClipContainerPtr cc)
{
	cc->setPositionBeats(this->smeToPosition->beat() + cc->timelineOffset());
	return EventCommand::applyEvent(cc);
}


QUuid ChangeVolumeCommand::applyEvent(ClipContainerPtr cc)
{
	cc->setVolume(qint16(this->rVolume));
	return EventCommand::applyEvent(cc);
}
