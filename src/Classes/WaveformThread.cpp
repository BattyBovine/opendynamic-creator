#include "WaveformThread.h"

WaveformThread::WaveformThread(AudioClipContainerPtr clip, qreal width, qreal height, qreal scale, quint32 resolution, quint32 tilecount)
{
	this->ccClip = clip;
	this->rWidth = width;
	this->rHeight = height;
	this->rScale = scale;
	this->iResolution = resolution;
	this->iTileCount = tilecount;
}

void WaveformThread::run()
{
	const char *pcm = this->ccClip->rawData();
	const quint64 datalength = this->ccClip->rawDataLength();
	const quint16 bytespersample = this->ccClip->bytesPerSample();
	const quint16 channels = this->ccClip->channelCount();
	if(datalength==0 || bytespersample==0 || channels==0)
		return;

	const int zeropoint = int(round(this->rHeight/2.0));
	const quint32 samplesize = (bytespersample*channels);
	const quint64 samplecount = (datalength/samplesize);

	const qreal samplesperpixel = qreal(samplecount) / (WT_MAX_TILE_LENGTH*this->iTileCount);

	QSettings settings;
	QString cachepath = settings.value(KEY_TEMP_FOLDER).toString();
	if(cachepath.isEmpty())
		return;
	cachepath.append(QString("/%1/").arg(this->ccClip->uuidString()));
	QDir path(cachepath);
	QFile tempofile(cachepath+(QString("/%1").arg(this->ccClip->tempo()*100.0)));
	if(!tempofile.exists()) {
		path.setFilter(QDir::NoDotAndDotDot);
		path.removeRecursively();
		path.mkpath(cachepath);
		tempofile.open(QFile::WriteOnly);
		tempofile.close();
	}
	cachepath.append(QString("%1/").arg(this->iResolution));
	path.setPath(cachepath);
	path.mkpath(cachepath);

	QColor waveformcolour = settings.value(KEY_WAVEFORM_COLOUR).value<QColor>();
	QPoint previouspoint(0,zeropoint);
	for(quint32 tile=0; tile<this->iTileCount; tile++) {
		QImage waveform;
		QFile out(path.absolutePath()+QString("/%1.bmp").arg(tile));
		if(out.exists()) {
			waveform.load(&out,"BMP");
			waveform.setColor(0, QColor(255,255,255,0).rgba());
			waveform.setColor(1, waveformcolour.rgba());
			emit(tileFinished(this->iResolution,int(tile),QPixmap::fromImage(waveform,Qt::MonoOnly)));
			continue;
		}
		waveform = QImage(WT_MAX_TILE_LENGTH, int(this->rHeight), QImage::Format_Mono);
		QPainter *paint = new QPainter(&waveform);
		paint->setBrush(QColor(255,255,255));
		paint->setPen(Qt::NoPen);
		paint->drawRect(0,0,WT_MAX_TILE_LENGTH,int(this->rHeight));
		paint->setPen(QColor(0,0,0));
		for(int x=0; x<WT_MAX_TILE_LENGTH; x++) {
			quint64 dataposition = quint64(round(((tile*WT_MAX_TILE_LENGTH)+x)*samplesperpixel)*samplesize);
			qint32 hivalue=0, lovalue=0;
			const quint32 roundedspp = quint32(round((samplesperpixel*x)/x));
			for(quint32 s=0; s<roundedspp; s++) {
				for(quint8 c=0; c<channels; c++) {
					quint64 dataoffset = dataposition+(s*samplesize)+(c*bytespersample);
					Q_ASSERT(dataposition<=datalength);
					quint32 samplevalue = 0;
					switch(bytespersample) {
					case 4:	samplevalue |= quint32(pcm[dataoffset+3] << 24);
					case 3:	samplevalue |= quint32(pcm[dataoffset+2] << 16);
					case 2:	samplevalue |= quint16(pcm[dataoffset+1] << 8);
					case 1:	samplevalue |= quint8 (pcm[dataoffset]);
					}
					int convertedvalue = 0;
					switch(bytespersample) {
					case 4:	convertedvalue = qint32(samplevalue); break;
					case 3:	convertedvalue = qint32(samplevalue); break;
					case 2:	convertedvalue = qint16(samplevalue); break;
					case 1:	convertedvalue = qint8 (samplevalue); break;
					}
					if(convertedvalue>hivalue || hivalue==0)
						hivalue = convertedvalue;
					if(convertedvalue<lovalue || lovalue==0)
						lovalue = convertedvalue;
				}
			}
			if(hivalue!=0) {
				QPoint nextpoint = this->intToPixel(hivalue,x,bytespersample,zeropoint);
				paint->drawLine(previouspoint,nextpoint);
				previouspoint=nextpoint;
			}
			if(lovalue!=0) {
				QPoint nextpoint = this->intToPixel(lovalue,x,bytespersample,zeropoint);
				paint->drawLine(previouspoint,nextpoint);
				previouspoint=nextpoint;
			}
		}
		delete paint;

		out.open(QFile::WriteOnly);
		waveform.save(&out);
		out.close();
		waveform.setColor(0, waveformcolour.rgba());
		waveform.setColor(1, QColor(255,255,255,0).rgba());
		emit(tileFinished(this->iResolution, int(tile), QPixmap::fromImage(waveform,Qt::MonoOnly)));
		previouspoint.setX(0);
	}
}
