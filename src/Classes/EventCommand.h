#ifndef EVENTCOMMAND_H
#define EVENTCOMMAND_H

#include <memory>
#include <QAudio>
#include <QUuid>

#include "Beat.h"
#include "ClipContainer.h"

class MusicEvent;
class StaticMusicEvent;


class EventCommand
{
	friend class MusicEvent;

public:
	virtual ~EventCommand(){}
	EventCommand(QUuid clip, Beat transitionlength=Beat(), MusicEvent *parent=Q_NULLPTR) {
		this->uuidAffectedClip = clip;
		this->beatTransitionLength=transitionlength;
		this->setParent(parent);
	}
	virtual QUuid applyEvent(ClipContainerPtr) { return QUuid(); }

	QUuid clip() const { return this->uuidAffectedClip; }

protected:
	void setParent(MusicEvent *p) { this->meParent=p; }
	QUuid uuidAffectedClip;
	MusicEvent *meParent=Q_NULLPTR;
	Beat beatTransitionLength;
};
typedef QList<EventCommandPtr> EventCommandList;

class JumpToMarkerCommand : public EventCommand
{
public:
	JumpToMarkerCommand(QUuid clip, Beat transitionlength=Beat()) : EventCommand(clip,transitionlength){}
	JumpToMarkerCommand(StaticMusicEventPtr sme, QUuid clip, Beat transitionlength=Beat()) : EventCommand(clip,transitionlength) { this->smeToPosition=sme; }
	QUuid applyEvent(ClipContainerPtr) override;
private:
	StaticMusicEventPtr smeToPosition;
};

class ChangeVolumeCommand : public EventCommand
{
public:
	ChangeVolumeCommand(QUuid clip, Beat transitionlength=Beat()) : EventCommand(clip,transitionlength){}
	ChangeVolumeCommand(qreal v, QUuid clip, Beat transitionlength=Beat()) : EventCommand(clip,transitionlength) { this->setVolume(v); }
	ChangeVolumeCommand(int d, QUuid clip, Beat transitionlength=Beat()) : EventCommand(clip,transitionlength) { this->setVolume(QAudio::convertVolume(d, QAudio::DecibelVolumeScale, QAudio::LogarithmicVolumeScale)); }
	void setVolume(qreal v) { this->rVolume=v; }
	QUuid applyEvent(ClipContainerPtr) override;
private:
	qreal rVolume = 0.0;
};

#endif // EVENTCOMMAND_H
