#ifndef CLIPPLAYER_H
#define CLIPPLAYER_H

#include <QDebug>

#include <QObject>
#include <QUuid>
#include <QAudioOutput>
#include <QBuffer>
#include <QHash>

#include "Classes/MusicEventWorker.h"


class SongPlayer;

typedef QHash<QUuid,QAudioOutput*> PlayerHash;
typedef QHash<QUuid,QBuffer*> BufferHash;
typedef QHash<QUuid,ClipContainerPtr> ClipHash;
class ClipPlayer : public QObject
{
	Q_OBJECT
public:
	ClipPlayer(ClipContainerPtr parentclip, QObject *parent=Q_NULLPTR);
	~ClipPlayer();
	bool addClipData(ClipContainerPtr);
	bool configureEventWorker(AudioClipContainerPtr);

public slots:
	void play(const SongPlayer*);
	void pause() { foreach(QAudioOutput *player, this->hashAudioPlayers) player->suspend(); }
	void stop();
	void updateVolume();

private slots:
	void setNextEvent();
	void stopEventThread();
	void playerState(QAudio::State);
	void setClipPositions();

signals:
	void finished();

private:
	void startEventThread();

	PlayerHash hashAudioPlayers;
	BufferHash hashDataBuffers;
	ClipHash hashClips;

	QUuid uuidMainClip;
	MusicEventWorker *mewEvents = Q_NULLPTR;
	qreal rGlobalVolume = 1.0;

	QTimer timerPlayerMarkerPosition;
	StaticMusicEventList::ConstIterator smeNextEvent = Q_NULLPTR;
};

#endif // CLIPPLAYER_H
