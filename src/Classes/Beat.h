#ifndef BEAT_H
#define BEAT_H

#include <QtGlobal>

class Beat
{
#define WHOLE_NOTE_TICKS 256.0
public:
	Beat(int tick=0) { this->iTick=tick; }

	void setTick(int b) { this->iTick=b; }
	int tick() { return this->iTick; }

	static Beat measures(int measures, quint8 beatspermeasure, quint8 beatunit) {
		int ticks = int(WHOLE_NOTE_TICKS*measures);
		qreal ratio = (beatspermeasure/qreal(beatunit));
		return Beat(int(round(ticks*ratio)));
	}
	static Beat wholeNote() { return Beat(int(WHOLE_NOTE_TICKS)); }
	static Beat halfNote() { return Beat(int(WHOLE_NOTE_TICKS/2)); }
	static Beat quarterNote() { return Beat(int(WHOLE_NOTE_TICKS/4)); }
	static Beat eighthNote() { return Beat(int(WHOLE_NOTE_TICKS/8)); }
	static Beat sixteenthNote() { return Beat(int(WHOLE_NOTE_TICKS/16)); }
	static Beat fromUnit(quint8 u) { return Beat(int(WHOLE_NOTE_TICKS/u)); }

	static Beat fromTimelinePosition(qreal pos, qreal measurespacing, quint8 beatspermeasure, quint8 beatunit, quint8 snap) { return Beat(int((round(((pos/measurespacing)*(beatspermeasure/qreal(beatunit)))*snap)/snap)*WHOLE_NOTE_TICKS)); }
	qreal toTimelinePosition(qreal measurespacing, quint8 beatspermeasure, quint8 beatunit) const { return (this->iTick*beatunit/WHOLE_NOTE_TICKS)*(measurespacing/beatspermeasure); }
	static Beat fromSeconds(qreal secs, qreal tempo, quint8 beatunit) { return Beat(int(round(secs*(tempo/60.0)/beatunit*WHOLE_NOTE_TICKS))); }
	qreal toSeconds(qreal tempo, quint8 beatunit) const { return (this->iTick*(60.0/tempo)*beatunit/WHOLE_NOTE_TICKS); }
	unsigned int measureCount(quint8 beatspermeasure, quint8 beatunit) const { return unsigned(ceil((this->iTick/WHOLE_NOTE_TICKS)*(beatunit/qreal(beatspermeasure)))); }

	Beat operator+(const Beat &b) const	{ return Beat(iTick+b.iTick); }
	Beat operator-(const Beat &b) const	{ return Beat(iTick-b.iTick); }
	Beat operator*(const Beat &b) const	{ return Beat(iTick*b.iTick); }
	Beat operator/(const Beat &b) const	{ return Beat(iTick/b.iTick); }

	Beat &operator+=(const Beat &b)	{ iTick+=b.iTick; return *this; }
	Beat &operator-=(const Beat &b)	{ iTick-=b.iTick; return *this; }
	Beat &operator*=(const Beat &b)	{ iTick*=b.iTick; return *this; }
	Beat &operator/=(const Beat &b)	{ iTick/=b.iTick; return *this; }

	bool operator==(const Beat &b) const	{ return (iTick==b.iTick); }
	bool operator!=(const Beat &b) const	{ return !(*this==b); }
	bool operator<(const Beat &b) const		{ return (iTick<b.iTick); }
	bool operator<=(const Beat &b) const	{ return (iTick<=b.iTick); }
	bool operator>(const Beat &b) const		{ return (iTick>b.iTick); }
	bool operator>=(const Beat &b) const	{ return (iTick>=b.iTick); }

	friend bool operator<=(const int &a, const Beat &b)	{ return (a<=b.iTick); }
	friend bool operator>=(const int &a, const Beat &b)	{ return (a>=b.iTick); }

private:
	int iTick;
};

#endif // BEAT_H
