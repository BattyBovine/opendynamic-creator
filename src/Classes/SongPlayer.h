#ifndef SONGPLAYER_H
#define SONGPLAYER_H

#include <QDebug>

#include <memory>

#include <QObject>
#include <QUuid>
#include <QSettings>
#include <QQueue>

#include "PreferencesDialog.h"
#include "ClipContainer.h"
#include "Classes/ClipPlayer.h"
#include "Widgets/MusicTreeView.h"


class SongPlayer : public QObject
{
	Q_OBJECT
public:
	enum Error {
		SP_OK,
		SP_NO_CLIPS,
		SP_INVALID_TRACK,
		SP_INVALID_DEVICE,
		SP_INVALID_ACTIVE_SEGMENT
	};

	explicit SongPlayer(ClipContainerPtr clip, QObject *parent=Q_NULLPTR);
	explicit SongPlayer(QObject *parent=Q_NULLPTR) : QObject(parent){}
	~SongPlayer();

public slots:
	Error setClip(ClipContainerPtr);

	Error playSong(const QUuid &startclip);
	Error playSong();
	void pauseSong();
	void stopSong();
//	void applyEvent(MusicEvent*);

signals:
	void finished();

private:
	Error searchItemHierarchy(ClipContainerPtr);
//	ClipPlayer* createClipPlayer(const QUuid&);

	QUuid uuidActiveClip = QUuid();
	QHash<QUuid,ClipContainerPtr> hashClips;
	QMultiHash<QUuid,QUuid> hashGroupMap;

//	ClipPlayer *cpActiveSegment = Q_NULLPTR;
//	ClipPlayer *cpTransitionSegment = Q_NULLPTR;
//	QQueue<ClipPlayer*> queueSegments;

	QScopedPointer<QAudioOutput> aoTestOutput;
};

#endif // SONGPLAYER_H
