#include "ClipContainer.h"
#include "MusicEventWorker.h"

#define DR_WAV_IMPLEMENTATION
#include <Libraries/dr_wav.h>
#undef DR_WAV_IMPLEMENTATION

ClipContainer::ClipContainer(ClipContainerPtr parent)
{
	this->uuidUnique = QUuid::createUuid();
	this->ccParent = parent;
}

ClipContainer::ClipContainer(const ClipContainer &c)
{
	this->copy(c);
}

ClipContainer &ClipContainer::operator=(const ClipContainer &c)
{
	this->copy(c);
	return *this;
}

void ClipContainer::copy(const ClipContainer &c)
{
	this->uuidUnique = c.uuidUnique;
	this->iSampleRate = c.iSampleRate;
	this->iChannelCount = c.iChannelCount;
	this->iLowerBitrate = c.iLowerBitrate;
	this->iNominalBitrate = c.iNominalBitrate;
	this->iUpperBitrate = c.iUpperBitrate;
	this->iBitrateWindow = c.iBitrateWindow;
	this->iBytesPerSample = c.iBytesPerSample;
	this->rLengthSeconds = c.rLengthSeconds;
	this->beatLength = c.beatLength;
	this->iVolume = c.iVolume;
	this->smelEvents = c.smelEvents;
	this->ccParent = c.ccParent;
}

void ClipContainer::removeFromParent()
{
	if(this->ccParent)
		this->ccParent->removeChild(this->uuidUnique);
	this->ccParent = Q_NULLPTR;
}

QAudioOutput *ClipContainer::createPlayer()
{
	QAudioFormat format;
	format.setSampleRate(int(this->sampleRate()));
	format.setChannelCount(this->channelCount());
	format.setSampleSize(this->bytesPerSample()*CHAR_BIT);
	format.setCodec("audio/pcm");
	format.setByteOrder(QAudioFormat::LittleEndian);
	format.setSampleType(QAudioFormat::SignedInt);

	QSettings settings;
	QList<QAudioDeviceInfo> devices = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
	int devicesetting = settings.value(KEY_OUTPUT_DEVICE).toInt();
	if(devices[devicesetting].isFormatSupported(format)) {
		QAudioOutput *out = new QAudioOutput(devices[devicesetting], format);
		out->setVolume(this->mixedVolumePercent());
		return out;
	}

	return Q_NULLPTR;
}

void ClipContainer::advanceBytePosition(qint64 add)
{
	this->iPositionBytes += add;
	this->rPositionSeconds = ClipContainer::bytesToSeconds(this->iPositionBytes, this->iSampleRate, this->iBytesPerSample, this->iChannelCount);
	emit(secondsPositionChanged(this->rPositionSeconds));
}


AudioClipContainer::AudioClipContainer(ClipContainerPtr parent) :
	ClipContainer(parent)
{
	if(this->ccParent && this->ccParent->type()==ClipContainerType::GROUP) {
		ClipGroupContainerPtr cgc = std::static_pointer_cast<ClipGroupContainer>(parent);
		connect(this, SIGNAL(destroyed()), cgc.get(), SLOT(childDeleted()));
	}
	this->bLoop = true;
}
void AudioClipContainer::copy(const ClipContainer &c)
{
	ClipContainer::copy(c);
	const AudioClipContainer &acc = static_cast<const AudioClipContainer&>(c);
	this->urlFilePath = acc.urlFilePath;
	this->baPCMData = acc.baPCMData;
}

void AudioClipContainer::setTimeInfo(qreal t, quint8 b, quint8 u)
{
	ClipContainer::setTimeInfo(t,b,u);
	this->setBeatLength(Beat::fromSeconds(this->rLengthSeconds, this->rTempo, this->iBeatUnit));
}

void AudioClipContainer::setParentClip(ClipContainerPtr parent, ClipContainerPtr child)
{
	ClipContainer::setParentClip(parent, child);
	parent->addChild(child);
}

ClipContainer::Error AudioClipContainer::loadAudioFile(QUrl file)
{
	if(!this->loadWav(file) && !this->loadVorbis(file))
		return ClipContainer::Error::FORMAT_UNRECOGNIZED;
	this->setBeatLength(Beat::fromSeconds(this->rLengthSeconds, this->rTempo, this->iBeatUnit));
	this->loop.position = this->loop.length = this->beatLength;
	return ClipContainer::Error::OK;
}

bool AudioClipContainer::loadWav(QUrl file)
{
	drwav wav;
	if(!drwav_init_file(&wav, file.toLocalFile().toStdString().c_str()))
		return false;
	this->urlFilePath = file;

	this->iSampleRate = wav.sampleRate;
	this->iChannelCount = wav.channels;
	this->iLowerBitrate = this->iNominalBitrate = this->iUpperBitrate = this->iBitrateWindow = 0;
	this->iBytesPerSample = wav.bytesPerSample;

	char *pcmbuffer = new char[wav.totalSampleCount*wav.bytesPerSample*wav.channels];
	quint64 bytecount = drwav_read_raw(&wav, wav.totalSampleCount*wav.bytesPerSample, pcmbuffer);
	this->baPCMData.clear();
	this->baPCMData.append(pcmbuffer, int(bytecount));
	delete[] pcmbuffer;
	drwav_uninit(&wav);

	this->rLengthSeconds = this->baPCMData.size() / qreal(this->iSampleRate * this->iBytesPerSample * this->iChannelCount);

	return true;
}

bool AudioClipContainer::loadVorbis(QUrl file)
{
	OggVorbis_File vorb;
	if(ov_fopen(file.toLocalFile().toStdString().c_str(), &vorb) < 0)
		return false;
	this->urlFilePath = file;

	this->iSampleRate = vorb.vi->rate;
	this->iChannelCount = vorb.vi->channels;
	this->iLowerBitrate = vorb.vi->bitrate_lower;
	this->iNominalBitrate = vorb.vi->bitrate_nominal;
	this->iUpperBitrate = vorb.vi->bitrate_upper;
	this->iBitrateWindow = vorb.vi->bitrate_window;
	this->iBytesPerSample = 2;

	this->baPCMData.clear();
	char pcmbuffer[4096];
	int section;
	bool eof = false;
	while(!eof) {
		long retval = ov_read(&vorb, pcmbuffer, sizeof(pcmbuffer), 0, 2, 1, &section);
		if(retval==0)
			eof = true;
		else if(retval<0)
			qDebug() << QString("Error decoding Ogg Vorbis data; attempting to ignore...");
		else
			this->baPCMData.append(pcmbuffer, retval);
	}
	ov_clear(&vorb);

	this->rLengthSeconds = this->baPCMData.size() / qreal(this->iSampleRate * this->iBytesPerSample * this->iChannelCount);

	return true;
}

qint64 AudioClipContainer::readData(char *data, qint64 length)
{
	QByteArray out;
	switch(this->iBytesPerSample) {
	case 2: {
		QList<qint16> data16 = this->read16BitData(length);
		QDataStream outstream(&out, QIODevice::WriteOnly);
		outstream.setByteOrder(QDataStream::LittleEndian);
		foreach(qint16 databytes, data16)
			outstream << databytes;
	}	break;
	default:
		break;
	}
	memcpy(data, out.data(), size_t(length));
	return length;
}

QList<qint16> AudioClipContainer::read16BitData(qint64 length)
{
	QByteArray bytes = this->pullBytesRecursively(length);
	QDataStream in(bytes);
	in.setByteOrder(QDataStream::LittleEndian);
	QList<qint16> out;
	while(!in.atEnd()) {
		qint16 outbytes;
		in >> outbytes;
		out << qint16(std::max<qint64>(INT16_MIN, std::min<qint64>(INT16_MAX, qint64(round(outbytes * this->volumePercent())))));
	}
	return out;
}

QByteArray AudioClipContainer::pullBytesRecursively(qint64 remainingbytes)
{
	QByteArray pulledbytes;
	const qint64 pcmlength = this->baPCMData.length();

	const qint64 loopend = ClipContainer::secondsToBytes(this->loop.position.toSeconds(this->rTempo, this->iBeatUnit), this->iSampleRate, this->iBytesPerSample, this->iChannelCount);
	const qint64 bytesleftinstream = std::min<qint64>(pcmlength - this->iPositionBytes, loopend - this->iPositionBytes);

	if(this->iPositionBytes < 0)
	{
		const qint64 bytesfromstart = abs(this->iPositionBytes);
		if(remainingbytes <= bytesfromstart) {	// If we're far enough from clip start to retrieve only zero bytes, send only zero bytes
			pulledbytes.fill(0, int(remainingbytes));
		} else {	// If we need to pad some zeroes before getting the first samples, do so
			pulledbytes.fill(0, int(bytesfromstart));
			pulledbytes.append(this->baPCMData.mid(0, int(remainingbytes - bytesfromstart)));
		}
		this->advanceBytePosition(remainingbytes);
	} else {
		if(remainingbytes <= bytesleftinstream) { // If there are enough bytes in the PCM data to provide the full sample request, do so
			pulledbytes.append(this->baPCMData.mid(int(this->iPositionBytes), int(remainingbytes)));
			this->advanceBytePosition(remainingbytes);
		} else {	// If there are not enough bytes remaining, loop back to the start and pull bytes from the beginning
			const qint64 firstbytecount = remainingbytes - bytesleftinstream;
			const qint64 secondbytecount = remainingbytes - firstbytecount;
			pulledbytes.append(this->baPCMData.mid(int(this->iPositionBytes), int(firstbytecount)));
			if(bLoop) {
				this->setPositionBeats(this->loop.position - this->loop.length);
				pulledbytes.append(this->pullBytesRecursively(secondbytecount));
			} else {
				this->advanceBytePosition(firstbytecount);
			}
		}
	}
	return pulledbytes;
}


void ClipGroupContainer::copy(const ClipContainer &c)
{
	ClipContainer::copy(c);
	const ClipGroupContainer &cgc = static_cast<const ClipGroupContainer&>(c);
	this->lChildClips = cgc.lChildClips;
}

void ClipGroupContainer::setPositionSeconds(qreal s)
{
	ClipContainer::setPositionSeconds(s);

	foreach(ClipContainerPtr clip, this->lChildClips)
		clip->setPositionSeconds(s + (this->beatTimelineOffset.toSeconds(this->rTempo, this->iBeatUnit) - clip->timelineOffset().toSeconds(this->rTempo, this->iBeatUnit)));
}

void ClipGroupContainer::addChild(ClipContainerPtr child)
{
	this->lChildClips.append(child);

	this->beatTimelineOffset = std::min(this->beatTimelineOffset, child->timelineOffset());
	this->beatLength = std::max(this->beatLength, child->beats() + this->beatTimelineOffset);
	this->rLengthSeconds = this->beatLength.toSeconds(this->rTempo, this->iBeatUnit);

	this->iSampleRate = std::max(child->sampleRate(), this->iSampleRate);
	this->iBytesPerSample = std::max(child->bytesPerSample(), this->iBytesPerSample);
	this->iChannelCount = std::max(child->channelCount(), this->iChannelCount);
	this->iLowerBitrate = std::min(child->bitrateLower(), this->iLowerBitrate);
	this->iNominalBitrate = std::max(child->bitrateNominal(), this->iNominalBitrate);
	this->iUpperBitrate = std::max(child->bitrateUpper(), this->iUpperBitrate);
	this->iBitrateWindow = std::max(child->bitrateWindow(), this->iBitrateWindow);

	connect(child.get(), SIGNAL(timelineOffsetChanged(Beat)), this, SLOT(recalculateTimelineBounds()));
}

void ClipGroupContainer::removeChild(QUuid &uuid)
{
	ClipContainerPtr remove = Q_NULLPTR;
	foreach(ClipContainerPtr child, this->lChildClips) {
		if(child->uuid() == uuid) {
			remove = child;
			break;
		}
	}
	if(remove)
		this->lChildClips.removeOne(remove);
}

void ClipGroupContainer::recalculateTimelineBounds()
{
	this->beatTimelineOffset = this->beatLength = Beat();
	foreach(ClipContainerPtr clip, this->lChildClips)
	{
		this->beatTimelineOffset = std::max(this->beatTimelineOffset, clip->timelineOffset());
		this->beatLength = std::max(this->beatLength, clip->beats()-clip->timelineOffset());
	}
}

ClipContainer::Error ClipGroupContainer::loadChildClipInfo(const ClipContainer &child)
{
	this->iSampleRate = std::max(this->iSampleRate, child.sampleRate());
	this->iChannelCount = std::max(this->iChannelCount, child.channelCount());
	this->iLowerBitrate = std::max(this->iLowerBitrate, child.bitrateLower());
	this->iNominalBitrate = std::max(this->iNominalBitrate, child.bitrateNominal());
	this->iUpperBitrate = std::max(this->iUpperBitrate, child.bitrateUpper());
	this->iBitrateWindow = std::max(this->iBitrateWindow, child.bitrateWindow());
	this->iBytesPerSample = std::max(this->iBytesPerSample, child.bytesPerSample());

	return ClipContainer::Error::OK;
}

qint64 ClipGroupContainer::readData(char *data, qint64 length)
{
	if(this->lChildClips.length()) {
		QByteArray out;
		switch(this->iBytesPerSample) {
		case 2: {
			QList<qint16> data16 = this->read16BitData(length);
			QDataStream outstream(&out, QIODevice::WriteOnly);
			outstream.setByteOrder(QDataStream::LittleEndian);
			foreach(qint16 databytes, data16)
				outstream << qint16(std::max<qint64>(INT16_MIN, std::min<qint64>(INT16_MAX, qint64(round(databytes * this->volumePercent())))));
		}	break;
		default:
			break;
		}
		memcpy(data, out.data(), size_t(length));
		return length;
	}
	memset(data, 0, quint64(length));
	return length;
}

qint64 ClipGroupContainer::bytesAvailable() const
{
	const Beat totallength = this->beatTimelineOffset + this->beatLength;
	return ClipContainer::secondsToBytes(totallength.toSeconds(this->rTempo, this->iBeatUnit), this->iSampleRate, this->iBytesPerSample, this->iChannelCount);
}

QList<qint16> ClipGroupContainer::read16BitData(qint64 length)
{
	const int clipcount = this->lChildClips.length();
	const ClipContainerPtr &firstclip = this->lChildClips.first();
	QList<qint16> mixresult = firstclip->read16BitData(length);
	for(int i=1; i<clipcount; i++) {
		const ClipContainerPtr &clip = this->lChildClips[i];
		QList<qint16> clipdata = clip->read16BitData(length);
		const int mixsize = std::min(mixresult.length(), clipdata.length());
		for(int j=0; j<mixsize; j++)
			mixresult[j] = qint16(std::max<int>(INT16_MIN, std::min<int>(mixresult[j]+clipdata[j], INT16_MAX)));
	}
	return mixresult;
}

void ClipGroupContainer::rewind()
{
	qreal seconds = this->beatTimelineOffset.toSeconds(this->rTempo, this->iBeatUnit);
	foreach(ClipContainerPtr child, this->lChildClips)
		child->setPositionSeconds(seconds);
}
