#ifndef MUSICEVENTWORKER_H
#define MUSICEVENTWORKER_H

#include <QDebug>

#include <QThread>
#include <QMutex>
#include <QBuffer>

#include "Beat.h"
#include "StaticMusicEvent.h"
#include "ClipContainer.h"


class MusicEventWorker : public QThread
{
	Q_OBJECT
public:
	MusicEventWorker(ClipContainerPtr cc, QBuffer *buf, QObject *parent=Q_NULLPTR) : QThread(parent)
	{
		this->ccParent = cc;
		if(cc->type()==ClipContainerType::AUDIO) {
			this->bufferClip = buf;
			AudioClipContainerPtr acc = std::static_pointer_cast<AudioClipContainer>(cc);
			this->iOffsetPos = qint64(round(ClipContainer::secondsToBytes(acc->timelineOffset().toSeconds(acc->tempo(),acc->beatUnit()),
																		  acc->sampleRate(),
																		  acc->bytesPerSample(),
																		  acc->channelCount())));
			this->iBeatLength = quint32(floor(ClipContainer::secondsToBytes(Beat::fromUnit(acc->beatUnit()).toSeconds(acc->tempo(),acc->beatUnit()),
																			acc->sampleRate(),
																			acc->bytesPerSample(),
																			acc->channelCount())));
			this->iMeasureLength = this->iBeatLength * acc->beatsPerMeasure();
		}
	}
public slots:
	void run() override {
		if(!this->bufferClip || !this->bufferClip->isOpen()) {
			emit(invalidBuffer());
			return;
		}
		while(this->bufferClip->isOpen() && !this->bStopped) {
			const quint64 bufferpos = quint64(this->bufferClip->pos());
			if(bufferpos >= this->iPosTarget) {
				emit(musicEvent(this->meEvent));
				this->iPosTarget = UINT64_MAX;
			}
			const qint64 bufferoffsetpos = qint64(bufferpos+quint64(this->iOffsetPos));
			const qint64 beatcount = qint64(bufferoffsetpos / this->iBeatLength);
			if(beatcount>=0 && this->iBeatValue!=beatcount) {
				this->iBeatValue = qint16(beatcount);
				emit(beat(beatcount));
			}
			const qint64 measurecount = (bufferoffsetpos / this->iMeasureLength);
			if(measurecount>=0 && this->iMeasureValue!=measurecount) {
				qDebug() << QString::number(measurecount);
				this->iMeasureValue = qint16(measurecount);
				emit(measure(measurecount));
			}
		}
	}
	void setNextEvent(StaticMusicEvent *sme) {
		QMutexLocker(&this->mutexLock);
		this->meEvent = sme->musicEvent().get();
		this->iPosTarget = quint64(round(ClipContainer::secondsToBytes(sme->beat().toSeconds(this->ccParent->tempo(),this->ccParent->beatUnit()),
																	   this->ccParent->sampleRate(),
																	   this->ccParent->bytesPerSample(),
																	   this->ccParent->channelCount())));
	}
	void stop() {
		QMutexLocker(&this->mutexLock);
		this->bStopped = true;
	}
signals:
	void musicEvent(MusicEvent*);
	void beat(qint64);
	void measure(qint64);
	void invalidBuffer();
	void stopped();
private:
	ClipContainerPtr ccParent = Q_NULLPTR;
	QBuffer *bufferClip = Q_NULLPTR;
	qint64 iOffsetPos = INT64_MAX;
	quint32 iBeatLength = 0xFFFFFFFF;
	quint32 iMeasureLength = 0xFFFFFFFF;
	qint16 iBeatValue = -1;
	qint16 iMeasureValue = -1;

	QMutex mutexLock;
	MusicEvent *meEvent = Q_NULLPTR;
	quint64 iPosTarget = UINT64_MAX;
	bool bStopped = false;
};

#endif // MUSICEVENTWORKER_H
