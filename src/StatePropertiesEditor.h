#ifndef STATEPROPERTIESEDITOR_H
#define STATEPROPERTIESEDITOR_H

#include <QWidget>

namespace Ui {
class StatePropertiesEditor;
}

class StatePropertiesEditor : public QWidget
{
	Q_OBJECT

public:
	explicit StatePropertiesEditor(QWidget *parent = Q_NULLPTR);
	~StatePropertiesEditor();

private:
	Ui::StatePropertiesEditor *ui = Q_NULLPTR;
};

#endif // STATEPROPERTIESEDITOR_H
