#include "ClipEditorWidget.h"
#include "ui_ClipEditorWidget.h"

ClipEditorWidget::ClipEditorWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::ClipEditorWidget)
{
	ui->setupUi(this);
}

ClipEditorWidget::~ClipEditorWidget()
{
	delete ui;
}

void ClipEditorWidget::setClipEditor(ClipContainerPtr clip)
{
	if(clip) {
		this->widgetMixer = new MixerWidget(this);
		this->widgetMixer->attachClipContainer(clip);
		this->widgetMixer->setGroupMode(false);
		this->widgetTimeline = new TimelineWidget(/*clip, false, */this);
		this->widgetTimeline->setClip(clip);
		this->widgetTimeline->setReadOnly(false);
		connect(this->widgetMixer, SIGNAL(snapChanged(int)), this->widgetTimeline, SLOT(setBeatUnitSnapFromCombo(int)));
		ui->layoutClipEditor->addWidget(this->widgetMixer, 0, 0);
		ui->layoutClipEditor->addWidget(this->widgetTimeline, 0, 1);
	}
}
