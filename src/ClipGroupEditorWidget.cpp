#include "ClipGroupEditorWidget.h"
#include "ui_ClipGroupEditorWidget.h"

ClipGroupEditorWidget::ClipGroupEditorWidget(QWidget *parent) :
	QScrollArea(parent),
	ui(new Ui::ClipGroupEditorWidget)
{
	ui->setupUi(this);
}

ClipGroupEditorWidget::~ClipGroupEditorWidget()
{
	delete ui;
	if(this->twMasterTimeline) this->twMasterTimeline->deleteLater();
}



void ClipGroupEditorWidget::setClipGroupEditor(ClipContainerPtr cc)
{
	MixerWidget *mw = new MixerWidget(this);
	mw->attachClipContainer(cc);
	mw->setGroupMode(true);
	this->twMasterTimeline = new TimelineWidget(this);
	this->twMasterTimeline->setClip(cc);
	this->twMasterTimeline->setReadOnly(false);
	connect(mw, SIGNAL(snapChanged(int)), this->twMasterTimeline, SLOT(setBeatUnitSnapFromCombo(int)));
	ui->layoutClipGroup->addWidget(mw, 0, 0);
	ui->layoutClipGroup->addWidget(this->twMasterTimeline, 0, 1);

	if(cc->type()==ClipContainerType::GROUP) {
		const ClipGroupContainerPtr &cgc = std::static_pointer_cast<ClipGroupContainer>(cc);
		const QList<ClipContainerPtr> &clips = cgc->childClips();
		Beat maxoffset;
		for(ClipContainerPtr clip : clips) {
			maxoffset = std::min(maxoffset, clip->timelineOffset());
		}
		for(ClipContainerPtr clip : clips) {
			MixerWidget *mw = new MixerWidget(this);
			mw->attachClipContainer(clip);
			mw->setGroupMode(true);
			TimelineWidget *tw = new TimelineWidget(this);
			tw->setClip(clip);
			tw->setReadOnly(true);
			tw->setStartPadding(maxoffset);
			const int row = ui->layoutClips->rowCount();
			ui->layoutClips->addWidget(mw, row, 0);
			ui->layoutClips->addWidget(tw, row, 1);
			connect(mw, SIGNAL(snapChanged(int)), tw, SLOT(setBeatUnitSnapFromCombo(int)));
			connect(this->twMasterTimeline, SIGNAL(zoomChanged(qreal)), tw, SLOT(setZoom(qreal)));
		}
	}
}
