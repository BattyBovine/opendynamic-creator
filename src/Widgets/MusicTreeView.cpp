#include "Widgets/MusicTreeView.h"

void MusicTreeView::keyPressEvent(QKeyEvent *e)
{
	switch(e->key()) {
	case Qt::Key_Delete:
		this->deleteSelectedItems();
		break;
	}
}

void MusicTreeView::deleteSelectedItems()
{
	if(QMessageBox::warning(this, "Delete selected?", "Are you sure you want to delete the selected items?",
							QMessageBox::Yes|QMessageBox::No, QMessageBox::No)!=QMessageBox::Yes)
		return;
	QModelIndexList selectedindices = this->selectedIndexes();
	QList<QModelIndex> deletelist;
	if(selectedindices.size()>0) {
		for(int i=selectedindices.size()-1; i>=0; i--) {
			QModelIndex &index = selectedindices[i];
			deletelist << index;
		}
	}
	emit(selectionChanged(QItemSelection(),QItemSelection()));
	std::sort(deletelist.begin(), deletelist.end(), [](QModelIndex a,QModelIndex b){return a.row()>b.row();});
	const QStandardItemModel *model = qobject_cast<QStandardItemModel*>(this->model());
	for(const QModelIndex &index : deletelist) {
		QStandardItem *item = model->itemFromIndex(index);
		if(item->type()>=MusicItemType::MIT_BASE) {
			ClipContainerPtr cc = static_cast<BaseMusicItem*>(item)->clipContainer();
			if(cc) cc->removeFromParent();
		}
		qobject_cast<QStandardItemModel*>(this->model())->removeRow(index.row(), index.parent());
	}
}

void MusicTreeView::playMusic()
{
	QModelIndexList selectedindices = this->selectedIndexes();
	return;
}



QStringList MusicTreeViewModel::mimeTypes() const
{
	QStringList types;
	types << MIT_MIME;
	types << "text/uri-list";
	return types;
}

QMimeData *MusicTreeViewModel::mimeData(const QModelIndexList &indices) const
{
	QMimeData *mime = QStandardItemModel::mimeData(indices);
	QByteArray mimebytes;
	{
		QDataStream ds(&mimebytes, QIODevice::WriteOnly);
		ds << quint32(indices.size());
        for(QModelIndexList::const_iterator i=indices.begin(); i<indices.end(); i++) {
            QStandardItem *itemptr = this->itemFromIndex(*i);
			ds.writeRawData(reinterpret_cast<const char*>(&itemptr), sizeof(QStandardItem*));
		}
	}
	mime->setData(MIT_MIME, mimebytes);
	return mime;
}

bool MusicTreeViewModel::dropMimeData(const QMimeData *data, Qt::DropAction, int row, int, const QModelIndex &parent)
{
	QStandardItem *parentitem = this->itemFromIndex(parent);
	if(data->hasFormat(MIT_MIME)) {
		QByteArray datamime = data->data(MIT_MIME);
		QDataStream ds(&datamime, QIODevice::ReadOnly);
		quint32 itemcount;
		ds >> itemcount;
		quint32 bytelength = itemcount * sizeof(QStandardItem*);
		QStandardItem **items = static_cast<QStandardItem**>(malloc(bytelength));
		ds.readRawData(reinterpret_cast<char*>(items), static_cast<int>(bytelength));
		for(quint32 i=0; i<itemcount; i++) {
			QStandardItem *item = items[i];
			switch(item->type()) {
			case MusicItemType::MIT_TRACK: {
				TrackItem *trackitem = static_cast<TrackItem*>(item->clone());
				if(!parentitem) {
					ClipContainerPtr parentclip = static_cast<BaseMusicItem*>(parentitem)->clipContainer();
					ClipContainerPtr childclip = trackitem->clipContainer();
					childclip->setParentClip(parentclip, childclip);
					if(row>=0)			this->insertRow(row, trackitem);
					else				this->appendRow(trackitem);
				} else {
					if(item->row()>=0)	this->insertRow(item->row(), trackitem);
					else				this->appendRow(trackitem);
				}
			}	break;
			case MusicItemType::MIT_CLIPGROUP: {
				ClipGroupItem *clipgroupitem = static_cast<ClipGroupItem*>(item->clone());
				if(parentitem && parentitem->type()==MusicItemType::MIT_TRACK) {
					ClipContainerPtr parentclip = static_cast<BaseMusicItem*>(parentitem)->clipContainer();
					ClipContainerPtr childclip = clipgroupitem->clipContainer();
					childclip->setParentClip(parentclip, childclip);
					if(row>=0)          parentitem->insertRow(row, clipgroupitem);
					else                parentitem->appendRow(clipgroupitem);
				} else {
					if(item->row()>=0)  item->parent()->insertRow(item->row(), clipgroupitem);
					else                item->parent()->appendRow(clipgroupitem);
				}
			}	break;
			case MusicItemType::MIT_CLIP: {
				ClipItem *clipitem = static_cast<ClipItem*>(item->clone());
				if(parentitem) {
					ClipContainerPtr parentclip = static_cast<BaseMusicItem*>(parentitem)->clipContainer();
					ClipContainerPtr childclip = clipitem->clipContainer();
					childclip->setParentClip(parentclip, childclip);
					if(row>=0)			parentitem->insertRow(row, clipitem);
					else				parentitem->appendRow(clipitem);
				} else {
					if(item->row()>=0)	item->parent()->insertRow(item->row(), clipitem);
					else				item->parent()->appendRow(clipitem);
				}
			}	break;
			}
		}
		free(items);
		return true;
	}
	if(data->hasFormat("text/uri-list")) {
		// First, make sure the files were dropped into a valid container for clips
		QStandardItem *parentitem = Q_NULLPTR;
		if(parent.isValid())
			parentitem = this->parseMusicItemMimeData(this->itemFromIndex(parent));
		else
			qDebug() << QString("Can't drop onto root item; ignoring drop.");
		if(parentitem==Q_NULLPTR)
			return false;

		// Next, make sure the dropped files are valid audio data, and ignore any that aren't
		QStringList filepaths = QString(data->data("text/uri-list")).split(QRegExp("(\\r\\n?|\\n)+"));
		QStringList validaudiofiles;
		QMediaPlayer *mediatest = new QMediaPlayer();
		foreach(QString fileurl, filepaths) {
			if(fileurl.isEmpty())
				continue;
			mediatest->setMedia(QUrl(fileurl));
			if(mediatest->error()!=QMediaPlayer::NoError) {
				qDebug() << QString("Error loading media file '%1': %2").arg(fileurl).arg(mediatest->errorString());
				continue;
			}
			validaudiofiles << fileurl;
		}
		mediatest->deleteLater();
		emit(audioClipsDropped(this->indexFromItem(parentitem), validaudiofiles));
	}
	return false;
}
QStandardItem *MusicTreeViewModel::parseMusicItemMimeData(QStandardItem *item)
{
	if(!item) {
		qDebug() << QString("Can't drop onto root item; ignoring drop.");
		return Q_NULLPTR;
	}
	switch(item->type()) {
	case MusicItemType::MIT_TRACK:
	case MusicItemType::MIT_CLIPGROUP:
		return item;
	case MusicItemType::MIT_CLIP:
	default:
		if(item->parent())
			return this->parseMusicItemMimeData(item->parent());
		else
			qDebug() << QString("Could not find valid parent; ignoring drop.");
		break;
	}
	return Q_NULLPTR;
}



void BaseMusicItem::setTimeInfo(qreal t, quint8 b, quint8 u)
{
	const int childcount = this->rowCount();
	for(int i=0; i<childcount; i++) {
		if(this->child(i)->type() >= MusicItemType::MIT_BASE)
			static_cast<BaseMusicItem*>(this->child(i))->setTimeInfo(t,b,u);
	}
	if(this->ccClip) {
		this->ccClip->setTimeInfo(t,b,u);
		if(this->ccClip->type()==ClipContainerType::GROUP)
			std::static_pointer_cast<ClipGroupContainer>(this->ccClip)->recalculateTimelineBounds();
	}
}



ClipGroupItem::ClipGroupItem(QString t) : BaseMusicItem(t)
{
	this->setIcon(QIcon(":/icons/mixer"));

	ClipGroupContainerPtr cgc = std::make_shared<ClipGroupContainer>(ClipGroupContainer());
	cgc->setName(this->text());
	this->ccClip = cgc;
}

ClipContainerPtr ClipGroupItem::clipContainer()
{
	BaseMusicItem *track = static_cast<BaseMusicItem*>(this->parent());
	while(track && track->type()!=MIT_TRACK)
		track = static_cast<BaseMusicItem*>(track->parent());
	if(track) {
		track = static_cast<TrackItem*>(track);
		this->ccClip->setTimeInfo(track->tempo(), track->beatsPerMeasure(), track->beatUnit());
	}
	return this->ccClip;
}



ClipItem::ClipItem(QString t) : BaseMusicItem(t)
{
	this->setIcon(QIcon(":/icons/waveform"));
	this->createClipContainer();
}
void ClipItem::createClipContainer()
{
	this->ccClip = std::make_shared<AudioClipContainer>(AudioClipContainer());
	this->ccClip->setName(this->text());
}

void ClipItem::loadClip(QUrl c)
{
	if(!c.isLocalFile())
		return;
	if(this->ccClip) {
		AudioClipContainerPtr acc = std::static_pointer_cast<AudioClipContainer>(this->ccClip);
		acc->loadAudioFile(c);
	}
}
void ClipItem::loadClip(QString c)
{
	this->loadClip(QUrl(c));
}
