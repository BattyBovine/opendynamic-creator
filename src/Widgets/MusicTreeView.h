#ifndef MUSICTREEVIEW_H
#define MUSICTREEVIEW_H

#include <memory>

#include <QDebug>
#include <QTreeView>
#include <QStandardItemModel>
#include <QItemSelection>

#include <QMimeData>
#include <QKeyEvent>
#include <QAudioOutput>
#include <QMediaPlayer>
#include <QMessageBox>

#include "Classes/MusicEvent.h"
#include "Classes/ClipContainer.h"

#define MIT_MIME "Qt/MusicItemType"


enum MusicItemType
{
	MIT_BASE = Qt::UserRole+1000,
	MIT_TRACK,
	MIT_CLIP,
	MIT_CLIPGROUP
};


class BaseMusicItem : public QStandardItem
{
public:
	BaseMusicItem(QString t) { this->setText(t); }
	virtual int type() const override { return MusicItemType::MIT_BASE; }
	virtual QStandardItem *clone() const override
	{
		BaseMusicItem *bmi = new BaseMusicItem(*this);
		for(int i=0; i<this->rowCount(); i++)
			bmi->appendRow(this->child(i)->clone());
		bmi->ccClip = this->ccClip;
		return bmi;
	}

	virtual ClipContainerPtr clipContainer() { return this->ccClip; }

	virtual void setTempo(qreal t) { this->setTimeInfo(t, this->beatsPerMeasure(), this->beatUnit()); }
	virtual void setBeatsPerMeasure(quint8 b) { this->setTimeInfo(this->tempo(), b, this->beatUnit()); }
	virtual void setBeatUnit(quint8 b) { this->setTimeInfo(this->tempo(), this->beatsPerMeasure(), b); }
	virtual void setTimeInfo(qreal t, quint8 b, quint8 u);
//	virtual void setPlaybackSpeed(qreal s) { if(this->ccClip) this->ccClip->setPlaybackSpeed(s); }

	virtual qreal tempo() const { return this->ccClip ? this->ccClip->tempo() : 0.0; }
	virtual quint8 beatsPerMeasure() const { return this->ccClip ? this->ccClip->beatsPerMeasure() : 0; }
	virtual quint8 beatUnit() const { return this->ccClip ? this->ccClip->beatUnit() : 0; }
//	virtual qreal playbackSpeed() const { return this->ccClip ? this->ccClip->playbackSpeed() : 0.0; }

protected:
	ClipContainerPtr ccClip = Q_NULLPTR;
};

class TrackItem : public BaseMusicItem
{
public:
	TrackItem(QString t) : BaseMusicItem(t)
	{
		this->ccClip = std::make_shared<ClipGroupContainer>(ClipGroupContainer());
		this->setIcon(QIcon(":/icons/note"));
		this->setTempo(120.0);
		this->setBeatsPerMeasure(4);
		this->setBeatUnit(4);
	//	this->setPlaybackSpeed(1.0);
	}
	virtual int type() const override { return MusicItemType::MIT_TRACK; }
	virtual QStandardItem *clone() const override
	{
		TrackItem *ti = new TrackItem(*this);
		for(int i=0; i<this->rowCount(); i++)
			ti->appendRow(this->child(i)->clone());
		ti->ccClip = this->ccClip;
		return ti;
	}
};

class ClipGroupItem : public BaseMusicItem
{
public:
	ClipGroupItem(QString t);
	virtual int type() const override { return MusicItemType::MIT_CLIPGROUP; }
	virtual QStandardItem *clone() const override
	{
		ClipGroupItem *cgi = new ClipGroupItem(*this);
		for(int i=0; i<this->rowCount(); i++)
			cgi->appendRow(this->child(i)->clone());
		cgi->ccClip = this->ccClip;
		return cgi;
	}

	virtual ClipContainerPtr clipContainer() override;
};

class ClipItem : public BaseMusicItem
{
public:
	ClipItem(QString);
	virtual int type() const override { return MusicItemType::MIT_CLIP; }
	virtual QStandardItem *clone() const override
	{
		ClipItem *ci = new ClipItem(*this);
		for(int i=0; i<this->rowCount(); i++)
			ci->appendRow(this->child(i)->clone());
		ci->ccClip = this->ccClip;
		return ci;
	}

	void loadClip(QUrl);
	void loadClip(QString);

private:
	void createClipContainer();
};



class MusicTreeViewModel : public QStandardItemModel
{
	Q_OBJECT
public:
	QStringList mimeTypes() const;
	QMimeData *mimeData(const QModelIndexList&) const;
	bool dropMimeData (const QMimeData*, Qt::DropAction, int, int, const QModelIndex&);

signals:
	void audioClipsDropped(QModelIndex,QStringList);

private:
	QStandardItem *parseMusicItemMimeData(QStandardItem*);
};



class MusicTreeView : public QTreeView
{
	Q_OBJECT
public:
	explicit MusicTreeView(QWidget *parent=Q_NULLPTR):QTreeView(parent){}

	virtual Qt::DropActions supportedDropActions() const { return Qt::CopyAction | Qt::MoveAction; }

public slots:
	void deleteSelectedItems();
	void playMusic();

protected:
	void keyPressEvent(QKeyEvent*);
};

#endif // MUSICTREEVIEW_H
