#include "Widgets/TimelineWidget.h"

TimelineWidget::TimelineWidget(QWidget *parent) : QGraphicsView(parent)
{
	this->gsTimeline = new QGraphicsScene(this);
	this->setScene(this->gsTimeline);

	this->setTopSpacing(TW_DEFAULT_TOP_SPACING);
	this->setMeasureSpacing(TW_DEFAULT_MEASURE_SPACING);

	this->setFrameShape(Shape::NoFrame);
	this->setAttribute(Qt::WA_TranslucentBackground);
	this->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	this->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
	this->viewport()->setAutoFillBackground(false);

	this->pmiPlayMarker = new PlayMarkerItem(this->rMeasureSpacing, this->rTopSpacing);
	this->pmiPlayMarker->setTimelinePos(0.0);
	this->pmiPlayMarker->setFlag(QGraphicsItem::ItemIgnoresTransformations);
	this->gsTimeline->addItem(this->pmiPlayMarker);
}

TimelineWidget::~TimelineWidget()
{
	for(QList<InvertedLineItem*> list : this->mapMeasureLines) {
		for(InvertedLineItem *l : list) {
			delete l;
		}
	}
	for(EventMarkerItem *i : this->lEventMarkers)
		i->deleteLater();
	this->pmiPlayMarker->deleteLater();
	this->lsiLoopSegment->deleteLater();
	if(this->cbtiClip)
		this->cbtiClip->deleteLater();
}


void TimelineWidget::resizeEvent(QResizeEvent *e)
{
	this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
	this->setAlignment(Qt::AlignTop|Qt::AlignLeft);
	this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
	this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	if(this->cbtiClip)
		this->cbtiClip->setHeight(this->height()-this->horizontalScrollBar()->height());
	this->redrawStageElements();
	QGraphicsView::resizeEvent(e);
}

void TimelineWidget::mousePressEvent(QMouseEvent *e)
{
	this->beatMouseClickPos = Beat::fromTimelinePosition(this->mapToScene(e->pos()).x(), this->rMeasureSpacing, this->ccClip->beatsPerMeasure(), this->ccClip->beatUnit(), this->iBeatUnitSnap) - this->ccClip->timelineOffset();
	if(e->button()==Qt::LeftButton) {
		this->bClickMode = true;
		if(this->cbtiClip)
			this->beatClipItemStart = this->cbtiClip->timelineBeat();
	}
	QGraphicsView::mousePressEvent(e);
}

void TimelineWidget::mouseMoveEvent(QMouseEvent *e)
{
	if(this->bClickMode && !this->bReadOnly) {
		this->beatMouseMovePos = Beat::fromTimelinePosition(this->mapToScene(e->pos()).x(), this->rMeasureSpacing, this->ccClip->beatsPerMeasure(), this->ccClip->beatUnit(), this->iBeatUnitSnap);
		if(abs((this->beatMouseClickPos-this->beatMouseMovePos).tick()) >= Beat::fromUnit(this->iBeatUnitSnap))
			this->bMoveMode = true;
		if(this->cbtiClip && this->bMoveMode) {
			if(this->cbtiClip->isSelected()) {
				Beat offset = this->beatClipItemStart+(this->beatMouseMovePos-this->beatMouseClickPos);
				this->cbtiClip->setTimelinePos(offset, this->rMeasureSpacing);
				this->ccClip->setTimelineOffset(this->cbtiClip->timelineBeat());
			}
			foreach(EventMarkerItem *emi, this->lEventMarkers) {
				if(emi->isSelected())
					emi->setTimelinePos(this->beatMouseMovePos-this->tiRelativeMarkerParent->timelineBeat(), this->rMeasureSpacing, this->ccClip->beatsPerMeasure(), this->ccClip->beatUnit());
			}
		}
	}
	QGraphicsView::mouseMoveEvent(e);
}

void TimelineWidget::mouseReleaseEvent(QMouseEvent *e)
{
	switch(e->button()) {
	case Qt::LeftButton: {
		if(!this->bMoveMode && !this->bReadOnly) {
			this->ccClip->setPositionBeats(this->beatMouseClickPos);
			this->beatPlayMarker = this->beatMouseClickPos;
		}
		this->setViewportBounds();
	}	break;
	case Qt::RightButton:
		if(!this->bReadOnly) {
			MusicEventPtr event = std::make_shared<MusicEvent>(MusicEvent());
			if(this->ccClip->events().size()>0)
				event->addCommand(std::make_shared<JumpToMarkerCommand>(JumpToMarkerCommand(this->ccClip->events().first(), this->ccClip->uuid())));
			this->ccClip->addEvent(event, this->beatMouseClickPos-this->tiRelativeMarkerParent->timelineBeat());
		}
	default:
		break;
	}
	this->bClickMode = this->bMoveMode = false;
	QGraphicsView::mouseReleaseEvent(e);
}

void TimelineWidget::wheelEvent(QWheelEvent *e)
{
	if(this->bReadOnly)
		return;

	this->setTransformationAnchor(QGraphicsView::NoAnchor);
	this->setResizeAnchor(QGraphicsView::NoAnchor);

	qreal zoomdelta = 0.0;
	int scroll = e->pixelDelta().y();
	if(scroll==0)	zoomdelta = (e->angleDelta().y()/2400.0);
	else			zoomdelta = (scroll/20.0);
	this->setZoom(zoomdelta);
	e->accept();
}

void TimelineWidget::keyPressEvent(QKeyEvent *e)
{
	switch(e->key()) {
	case Qt::Key_I: {
		LoopSegment loop = this->ccClip->loopSegment();
		loop.length = loop.position-this->beatPlayMarker;
		this->ccClip->setLoopSegment(loop);
	}	break;
	case Qt::Key_O: {
		LoopSegment loop = this->ccClip->loopSegment();
		loop.length += (this->beatPlayMarker-loop.position);
		loop.position = this->beatPlayMarker;
		this->ccClip->setLoopSegment(loop);
	}	break;
	}
}


void TimelineWidget::addEventMarker(StaticMusicEventPtr sme)
{
	EventMarkerItem *emi = new EventMarkerItem(sme, this->rMeasureSpacing, this->rTopSpacing);
	emi->setTimelinePos(sme->beat(), this->rMeasureSpacing, this->ccClip->beatsPerMeasure(), this->ccClip->beatUnit());
	emi->setFlag(QGraphicsItem::ItemIgnoresTransformations);
	if(!this->bReadOnly)
		emi->setFlag(QGraphicsItem::ItemIsSelectable);
	emi->setParentItem(this->tiRelativeMarkerParent);
	this->lEventMarkers.append(emi);
}

void TimelineWidget::moveRelativeMarkers(Beat &b)
{
	this->tiRelativeMarkerParent->setTimelinePos(b, this->rMeasureSpacing, this->ccClip->beatsPerMeasure(), this->ccClip->beatUnit());
}

void TimelineWidget::createMeasureMarkers()
{
	QPen pen(this->palette().foreground().color());
	pen.setWidth(0);
	InvertedLineItem *line = new InvertedLineItem(0.0, this->rTopSpacing,
												  0.0, this->rTopSpacing+TW_MEASURE_MARKER_LENGTH);
	this->mapMeasureLines[1].append(line);
	const quint8 beatspermeasure = this->ccClip->beatsPerMeasure();
	unsigned int measurecount = this->ccClip->beats().measureCount(beatspermeasure, this->ccClip->beatUnit());
	for(unsigned int measure=0; measure<measurecount; measure++) {
		for(int beat=1; beat<=beatspermeasure; beat++) {
			qreal spacing = this->rMeasureSpacing/beatspermeasure;
			qreal xpos = (this->rMeasureSpacing*measure) + (spacing*beat);
			qreal markerlength = (TW_MEASURE_MARKER_LENGTH*TW_BEAT_MARKER_DELTA);
			if(beat!=beatspermeasure) {
				line = new InvertedLineItem(xpos, this->rTopSpacing,
											xpos, this->rTopSpacing+markerlength);
				this->mapMeasureLines[4].append(line);
			}
			spacing /= 2.0;
			this->createBeatMarkers(8, xpos-spacing, spacing, this->rScale, markerlength*TW_BEAT_MARKER_DELTA, pen);
		}
		line = new InvertedLineItem(this->rMeasureSpacing*(measure+1), this->rTopSpacing,
									this->rMeasureSpacing*(measure+1), this->rTopSpacing+TW_MEASURE_MARKER_LENGTH);
		this->mapMeasureLines[1].append(line);
	}
}
void TimelineWidget::createBeatMarkers(quint8 unit, qreal pos, qreal spacing, qreal scale, qreal length, QPen &pen)
{
	if(unit>=128) return;
	InvertedLineItem *subbeatline = new InvertedLineItem(pos, this->rTopSpacing,
														 pos, this->rTopSpacing+length);
	this->mapMeasureLines[unit].append(subbeatline);
	spacing /= 2.0;
	this->createBeatMarkers(unit*2, pos-spacing, spacing, scale, length*TW_BEAT_MARKER_DELTA, pen);
	this->createBeatMarkers(unit*2, pos+spacing, spacing, scale, length*TW_BEAT_MARKER_DELTA, pen);
}
void TimelineWidget::configureRelativeMarkers(ClipBaseTimelineItem* clipitem, const ClipContainerPtr& clip)
{
	// Create a parent for items that should move with the clip item
	if(!this->tiRelativeMarkerParent) {
		this->tiRelativeMarkerParent = new TimelineItem(this->rMeasureSpacing, this->rTopSpacing);
		this->tiRelativeMarkerParent->setTimelinePos(clipitem->timelineBeat(), this->rMeasureSpacing, clip->beatsPerMeasure(), clip->beatUnit());
		this->gsTimeline->addItem(this->tiRelativeMarkerParent);
		connect(clipitem, SIGNAL(moved(Beat&)), this, SLOT(moveRelativeMarkers(Beat&)));
	}

	// Configure loop marker item
	this->lsiLoopSegment = new LoopSegmentItem(this->rMeasureSpacing, this->rTopSpacing);
	this->lsiLoopSegment->setFlag(QGraphicsItem::ItemIgnoresTransformations);
	this->lsiLoopSegment->setClip(clip);
	this->lsiLoopSegment->setParentItem(this->tiRelativeMarkerParent);
	this->gsTimeline->addItem(this->lsiLoopSegment);
	connect(this->ccClip.get(), SIGNAL(loopSegmentChanged(LoopSegment)), this, SLOT(updateLoopMarkers()));
	this->updateLoopMarkers();

	// Configure event items
	if(!this->bReadOnly) {
		clipitem->setFlag(QGraphicsItem::ItemIsSelectable);
		StaticMusicEventList events = clip->events();
		foreach(StaticMusicEventPtr e, events)
			this->addEventMarker(e);
		connect(clip.get(), SIGNAL(eventAdded(StaticMusicEventPtr)), this, SLOT(addEventMarker(StaticMusicEventPtr)));
	}
}
void TimelineWidget::drawMeasureMarkers()
{
	QList<QGraphicsItem*> activelines = this->gsTimeline->items();
	if(!this->mapMeasureLines.contains(1)) return;
	if(!activelines.contains(this->mapMeasureLines[1].first())) {
		foreach(InvertedLineItem *measureline, this->mapMeasureLines[1])
			this->gsTimeline->addItem(measureline);
	}
	if(!this->mapMeasureLines.contains(4)) return;
	if(!activelines.contains(this->mapMeasureLines[4].first())) {
		foreach(InvertedLineItem *beatline, this->mapMeasureLines[4])
			this->gsTimeline->addItem(beatline);
	}

	qreal spacing = this->rMeasureSpacing/this->ccClip->beatsPerMeasure();
	quint8 unit = 8;
	while(true) {
		if(unit>=128) break;
		if(!this->mapMeasureLines.contains(unit)) return;
		spacing /= 2.0;
		if((spacing*this->rScale)<=TW_SUB_BEAT_MIN_SPACING) {
			if(activelines.contains(this->mapMeasureLines[unit].first())) {
				foreach(InvertedLineItem *beatline, this->mapMeasureLines[unit])
					this->gsTimeline->removeItem(beatline);
			}
		} else {
			if(!activelines.contains(this->mapMeasureLines[unit].first())) {
				foreach(InvertedLineItem *beatline, this->mapMeasureLines[unit])
					this->gsTimeline->addItem(beatline);
			}
		}
		unit*=2;
	}
}


void TimelineWidget::setZoom(qreal zoomdelta)
{
	QTransform transform = this->transform();
	qreal scale = transform.m11()+zoomdelta;
	scale = std::max(TW_MIN_SCALE, std::min(scale, TW_MAX_SCALE));
	transform.setMatrix(scale,				transform.m12(),	transform.m13(),
						transform.m21(),	transform.m22(),	transform.m23(),
						transform.m31(),	transform.m32(),	transform.m33());
	this->setTransform(transform);
	this->centerOn(this->pmiPlayMarker->x(),0.0);
	this->rScale = scale;
	this->redrawStageElements();

	emit(zoomChanged(zoomdelta));
}

void TimelineWidget::redrawStageElements()
{
	this->drawMeasureMarkers();
	if(this->cbtiClip)
		this->cbtiClip->setTimelineScale(this->rScale);
	if(this->lsiLoopSegment) {
		this->lsiLoopSegment->setTimelineScale(this->rScale);
		this->lsiLoopSegment->updateLoop();
	}
}

void TimelineWidget::setClip(ClipContainerPtr cc)
{
	this->ccClip=cc;

	if(this->cbtiClip)
		this->cbtiClip->deleteLater();

	ClipBaseTimelineItem* clipitem = Q_NULLPTR;
	switch(cc->type()) {
	case ClipContainerType::AUDIO:
		clipitem = new ClipTimelineItem(std::static_pointer_cast<AudioClipContainer>(cc),
										this->rMeasureSpacing,
										this->rTopSpacing+(TW_MEASURE_MARKER_LENGTH*TW_BEAT_MARKER_DELTA));
		break;
	case ClipContainerType::GROUP:
		clipitem = new ClipGroupTimelineItem(std::static_pointer_cast<ClipGroupContainer>(cc),
											 this->rMeasureSpacing,
											 this->rTopSpacing+(TW_MEASURE_MARKER_LENGTH*TW_BEAT_MARKER_DELTA));
	default:
		break;
	}
	clipitem->setFlag(QGraphicsItem::ItemIgnoresTransformations);
	this->gsTimeline->addItem(clipitem);

	this->configureRelativeMarkers(clipitem, cc);

	this->cbtiClip = clipitem;

	this->createMeasureMarkers();
	this->setViewportBounds();
	this->setZoom(-1000.0);

	connect(this->ccClip.get(), SIGNAL(secondsPositionChanged(qreal)), this, SLOT(movePlayMarkerToClipPos(qreal)));
}

void TimelineWidget::setGroupClip()
{
	if(this->cbtiClip)
		this->cbtiClip->deleteLater();

	if(this->ccClip && this->ccClip->type()==ClipContainerType::GROUP) {
		ClipGroupTimelineItem* clipgroupitem = new ClipGroupTimelineItem(std::static_pointer_cast<ClipGroupContainer>(this->ccClip),
																		 this->rMeasureSpacing,
																		 this->rTopSpacing+(TW_MEASURE_MARKER_LENGTH*TW_BEAT_MARKER_DELTA));
		clipgroupitem->setFlag(QGraphicsItem::ItemIgnoresTransformations);
		this->gsTimeline->addItem(clipgroupitem);
		this->cbtiClip = clipgroupitem;
	}

//	this->configureEventMarkers(ClipContainerPtr(new ClipContainer(QUrl(),clipgroupitem)));

	this->createMeasureMarkers();
	this->setViewportBounds();
	this->setZoom(-1000);
}

void TimelineWidget::setViewportBounds()
{
	const quint8 beatspermeasure = this->ccClip->beatsPerMeasure();
	const quint8 beatunit = this->ccClip->beatUnit();
	Beat startposition = this->ccClip->timelineOffset();
	const int endposition = int(((this->ccClip->beats()+this->ccClip->timelineOffset()).measureCount(beatspermeasure, beatunit) * this->rMeasureSpacing)
			- startposition.toTimelinePosition(this->rMeasureSpacing, beatspermeasure, beatunit));
	if(startposition>0)
		startposition.setTick(0);
	const qreal finalstartposition = std::min(startposition.toTimelinePosition(this->rMeasureSpacing, beatspermeasure, beatunit),
											  this->beatStartPadding.toTimelinePosition(this->rMeasureSpacing, beatspermeasure, beatunit));
	this->gsTimeline->setSceneRect(finalstartposition, 0, endposition, 100);
}


void TimelineWidget::movePlayMarkerToClipPos(qreal secs)
{
	Beat pos = Beat::fromSeconds(secs, this->ccClip->tempo(), this->ccClip->beatUnit());
	pos += this->ccClip->timelineOffset();
	this->pmiPlayMarker->setTimelinePos(pos.toTimelinePosition(this->rMeasureSpacing, this->ccClip->beatsPerMeasure(), this->ccClip->beatUnit()));
	this->centerOn(this->pmiPlayMarker->x(), 0.0);
}

void TimelineWidget::updateLoopMarkers()
{
	this->lsiLoopSegment->updateLoop();
	this->gsTimeline->update();
}
