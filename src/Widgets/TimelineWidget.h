#ifndef TIMELINEWIDGET_H
#define TIMELINEWIDGET_H

#include <memory>

#include <QGraphicsView>
#include <QOpenGLWidget>
#include <QScrollBar>
#include <QAction>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QTimer>

#include "GraphicsItems/PlayMarkerItem.h"
#include "GraphicsItems/LoopSegmentItem.h"
#include "GraphicsItems/EventMarkerItem.h"
#include "GraphicsItems/ClipTimelineItem.h"
#include "GraphicsItems/ClipGroupTimelineItem.h"
#include "GraphicsItems/InvertedLineItem.h"
#include "MusicTreeView.h"
#include "Classes/EventCommand.h"
#include "Classes/MusicEvent.h"

#define TW_MEASURE_MARKER_LENGTH	6.0
#define TW_BEAT_MARKER_DELTA		0.75
#define TW_SUB_BEAT_MIN_SPACING		6.0

#define TW_MIN_SCALE				0.2
#define TW_DEFAULT_MEASURE_SPACING	40.0
#define TW_MAX_SCALE				20.0
#define TW_DEFAULT_TOP_SPACING		5.0


class TimelineWidget : public QGraphicsView
{
	Q_OBJECT
public:
	explicit TimelineWidget(QWidget *parent=Q_NULLPTR);
	~TimelineWidget() override;
	ClipContainerPtr clip() const { return this->ccClip; }
	qreal measureSpacing() const { return this->rMeasureSpacing; }

public slots:
	void setClip(ClipContainerPtr);
	void setGroupClip();
	void setTopSpacing(qreal t) { this->rTopSpacing=t; }
	void setMeasureSpacing(qreal m) { this->rMeasureSpacing=m; }
	void setStartPadding(Beat p) { this->beatStartPadding=p; this->setViewportBounds(); }
	void setViewportBounds();
	void setZoom(qreal);

	void setReadOnly(bool r) { this->bReadOnly=r; }
	void setBeatUnitSnapFromCombo(int s) { this->iBeatUnitSnap=quint8(pow(2,s)); }
	void movePlayMarkerToClipPos(qreal);
	void updateLoopMarkers();

signals:
	void zoomChanged(qreal);

protected:
	virtual void resizeEvent(QResizeEvent*) override;
	virtual void mousePressEvent(QMouseEvent*) override;
	virtual void mouseMoveEvent(QMouseEvent*) override;
	virtual void mouseReleaseEvent(QMouseEvent*) override;
	virtual void wheelEvent(QWheelEvent*) override;
	virtual void keyPressEvent(QKeyEvent*) override;

private slots:
	void addEventMarker(StaticMusicEventPtr);

	void moveRelativeMarkers(Beat&);

private:
	void redrawStageElements();

	void createMeasureMarkers();
	void createBeatMarkers(quint8, qreal, qreal, qreal, qreal, QPen&);
	void configureRelativeMarkers(ClipBaseTimelineItem*, const ClipContainerPtr&);
	void drawMeasureMarkers();

	qreal posToSeconds(qreal pos) const { return pos/(this->rMeasureSpacing/this->ccClip->beatsPerMeasure())*(60.0/this->ccClip->tempo()); }
	qreal secondsToPos(qreal secs) const { return secs*(this->ccClip->tempo()/60.0)*(this->rMeasureSpacing/this->ccClip->beatsPerMeasure()); }

	QGraphicsScene *gsTimeline = Q_NULLPTR;
	ClipContainerPtr ccClip = Q_NULLPTR;
	PlayMarkerItem *pmiPlayMarker = Q_NULLPTR;
	ClipBaseTimelineItem *cbtiClip = Q_NULLPTR;
	TimelineItem *tiRelativeMarkerParent = Q_NULLPTR;
	LoopSegmentItem *lsiLoopSegment = Q_NULLPTR;
	QList<EventMarkerItem*> lEventMarkers;
	QMap<unsigned int,QList<InvertedLineItem*> > mapMeasureLines;

	qreal rScale = 1.0;
	qreal rTopSpacing = 0.0;
	qreal rMeasureSpacing = 0.0;
	bool bReadOnly = false;

	Beat beatPlayMarker;
	Beat beatMouseClickPos, beatMouseMovePos;
	Beat beatClipItemStart;
	Beat beatStartPadding;
	quint8 iBeatUnitSnap = 4;
	bool bClickMode = false;
	bool bMoveMode = false;
};

#endif // TIMELINEWIDGET_H
