#include "ClipBaseTimelineItem.h"

void ClipBaseTimelineItem::paint(QPainter *p, const QStyleOptionGraphicsItem*, QWidget*)
{
	p->setRenderHint(QPainter::HighQualityAntialiasing);
//	p->setRenderHint(QPainter::SmoothPixmapTransform);
	const QRectF mybounds = this->boundingRect();
//#ifndef QT_DEBUG
	p->setPen(Qt::NoPen);
	p->setBrush(QColor(0, 0, 255, 64));
	p->drawRect(mybounds);
//#endif
//	const int tilecount = this->mapWaveforms[this->iWaveformResolution].size();
//	if(tilecount>0) {
//		const qreal pixwidth = (mybounds.width()-1) / tilecount;
//		for(int i=0; i<tilecount; i++) {
//			const QPixmap &currentmap = this->mapWaveforms[this->iWaveformResolution][i];
//			const QRectF itemregion(pixwidth*i, mybounds.y(), pixwidth+0.75, mybounds.height());
//			const QRectF pixmapbounds(0, 0, WT_MAX_TILE_LENGTH, currentmap.height());
//			p->drawPixmap(itemregion, currentmap, pixmapbounds);
//#ifdef QT_DEBUG
//			p->setPen(QPen(QBrush(Qt::red),0.0));
//			p->setBrush(Qt::NoBrush);
//			p->drawRect(itemregion);
//#else
//			p->setPen(QColor(0, 0, 255));
//			p->setBrush(Qt::NoBrush);
//			p->drawRoundedRect(mybounds,2.0,2.0);
//#endif
//		}
//	}
}
