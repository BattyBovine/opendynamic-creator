#include "ClipGroupTimelineItem.h"

void ClipGroupTimelineItem::paint(QPainter *p, const QStyleOptionGraphicsItem*, QWidget*)
{
	p->setRenderHint(QPainter::HighQualityAntialiasing);
	const QRectF mybounds = this->boundingRect();
	p->setPen(Qt::NoPen);
	p->setBrush(QColor(0, 0, 255, 64));
	p->drawRoundedRect(mybounds,2.5,2.5);
	p->setPen(QColor(0, 0, 255));
	p->setBrush(Qt::NoBrush);
	p->drawRoundedRect(mybounds,2.5,2.5);
}

void ClipGroupTimelineItem::calculateCombinedClipLengths()
{
	this->rLength = (this->cgcClip->timelineOffset()+this->cgcClip->beats()).toTimelinePosition(this->rMeasureSpacing, this->cgcClip->beatsPerMeasure(), this->cgcClip->beatUnit());
	this->updateBoundingRect();
}
