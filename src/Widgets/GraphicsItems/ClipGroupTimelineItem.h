#ifndef CLIPGROUPTIMELINEITEM_H
#define CLIPGROUPTIMELINEITEM_H

#include <memory>

#include <QImage>
#include <QFile>
#include <QDir>
#include <QCoreApplication>

#include "ClipTimelineItem.h"
#include "Classes/ClipContainer.h"
#include "Classes/WaveformThread.h"


class ClipGroupTimelineItem : public ClipBaseTimelineItem
{
	Q_OBJECT
public:
	ClipGroupTimelineItem(const ClipGroupContainerPtr &cgc, qreal measurespacing, qreal topspacing) : ClipBaseTimelineItem(measurespacing, topspacing) {
		this->cgcClip = cgc;
		this->rMeasureSpacing = measurespacing;
		this->calculateCombinedClipLengths();
		this->setZValue(-10.0);
	}

//	virtual void setTimelinePos(Beat p, qreal s) override { TimelineItem::setTimelinePos(p,s,this->cgcClip->beatsPerMeasure(),this->cgcClip->beatUnit()); this->cgcClip->setTimelineOffset(this->timelineBeat()); this->updateBoundingRect(); }

//	void addClip(ClipContainerPtr &clip) { this->lClips.append(clip); this->calculateCombinedClipLengths(); }

	void setTimelineScale(qreal scale) override { ClipBaseTimelineItem::setTimelineScale(scale); this->updateBoundingRect(); }

protected:
	void paint(QPainter*,const QStyleOptionGraphicsItem*,QWidget*) override;

private:
	void calculateCombinedClipLengths();

	ClipGroupContainerPtr cgcClip;
};

#endif // CLIPGROUPTIMELINEITEM_H
