#include "EventMarkerItem.h"

void EventMarkerItem::paint(QPainter *p, const QStyleOptionGraphicsItem*, QWidget*)
{
	QSettings settings;
	p->setBrush(Qt::NoBrush);
	p->setPen(settings.value(KEY_EVENT_MARKER_COLOUR).value<QColor>());
	QVector<QPointF> polyline;
	const qreal topspacing = this->topSpacing();
	polyline << QPointF(0.0, 0.0)
			 << QPointF(-(topspacing/2.0), (topspacing/2.0))
			 << QPointF(0.0, topspacing)
			 << QPointF((topspacing/2.0), (topspacing/2.0))
			 << QPointF(0.0, 0.0);
	p->drawPolyline(QPolygonF(polyline));
	p->drawLine(QPointF(0.0, topspacing), QPointF(0.0, TLI_MAX_HEIGHT));
}
