#include "ClipTimelineItem.h"

void ClipTimelineItem::paint(QPainter *p, const QStyleOptionGraphicsItem*, QWidget*)
{
	p->setRenderHint(QPainter::HighQualityAntialiasing);
	p->setRenderHint(QPainter::SmoothPixmapTransform);
	const QRectF mybounds = this->boundingRect();
#ifndef QT_DEBUG
	p->setPen(Qt::NoPen);
	p->setBrush(QColor(0, 0, 255, 64));
	p->drawRoundedRect(mybounds,2.5,2.5);
#endif
	const int tilecount = this->mapWaveforms[this->iWaveformResolution].size();
	if(tilecount>0) {
		const qreal pixwidth = (mybounds.width()-1) / tilecount;
		for(int i=0; i<tilecount; i++) {
			const QPixmap &currentmap = this->mapWaveforms[this->iWaveformResolution][i];
			const QRectF itemregion(pixwidth*i, mybounds.y(), pixwidth+0.75, mybounds.height());
			const QRectF pixmapbounds(0, 0, WT_MAX_TILE_LENGTH, currentmap.height());
			p->drawPixmap(itemregion, currentmap, pixmapbounds);
#ifdef QT_DEBUG
			p->setPen(QPen(QBrush(Qt::red),0.0));
			p->setBrush(Qt::NoBrush);
			p->drawRect(itemregion);
#else
			p->setPen(QColor(0, 0, 255));
			p->setBrush(Qt::NoBrush);
			p->drawRoundedRect(mybounds,2.5,2.5);
#endif
		}
	}
}

void ClipTimelineItem::generateWaveform()
{
	if(this->ccClip->type()==ClipContainerType::AUDIO) {
		const quint8 zoomscale = quint8(round(this->rTimelineScale+0.5));
		if(zoomscale==this->iWaveformResolution || this->rHeight<1.0)
			return;
		this->iWaveformResolution = zoomscale;

		const qreal waveformlength = this->rLength * (this->iWaveformResolution+1);
		const int tilecount = int(ceil(waveformlength/WT_MAX_TILE_LENGTH));
		while(this->mapWaveforms[this->iWaveformResolution].size() < tilecount)
			this->mapWaveforms[this->iWaveformResolution].append(QPixmap());
		AudioClipContainerPtr acc = std::static_pointer_cast<AudioClipContainer>(this->ccClip);
		WaveformThread *thread = new WaveformThread(acc, this->rLength, this->rHeight, this->rTimelineScale,
													this->iWaveformResolution, quint32(tilecount));
		connect(thread, SIGNAL(tileFinished(quint32,int,QPixmap)), this, SLOT(getWaveformTile(quint32,int,QPixmap)));
		connect(thread, SIGNAL(finished()), this, SLOT(threadFinished()));
		thread->start();
	}
}
void ClipTimelineItem::getWaveformTile(quint32 resolution, int tile, QPixmap pix)
{
	this->mapWaveforms[resolution].replace(tile, pix);
	this->update(this->boundingRect());
}
void ClipTimelineItem::threadFinished()
{
	QObject::sender()->deleteLater();
}
