#include "TimelineItem.h"

TimelineItem::TimelineItem(qreal measurespacing, qreal topspacing)
{
	this->setZValue(1.0);
	this->setMeasureSpacing(measurespacing);
	this->setTopSpacing(topspacing);
	this->rectBounds = QRectF(QPointF(-this->rTopSpacing/2.0, 0.0), QPointF(this->rTopSpacing/2.0, TLI_MAX_HEIGHT));
}
