#ifndef TIMELINEITEM_H
#define TIMELINEITEM_H

#include <QGraphicsItem>
#include <QPainter>

#include "Classes/Beat.h"
#include "Classes/MusicEvent.h"

#define TLI_MAX_HEIGHT	2048.0


class TimelineItem : public QGraphicsObject
{
	Q_OBJECT
public:
	TimelineItem(qreal measurespacing, qreal topspacing);

	virtual QRectF boundingRect() const { return this->rectBounds; }

	virtual void setTimelinePos(Beat p, qreal s) { this->setTimelinePos(p,s,0,0); }
	virtual void setTimelinePos(Beat p, qreal s, quint8 b, quint8 u) { this->beatPosition=p; this->setTimelinePos(p.toTimelinePosition(s,b,u)); emit(moved(p)); }
	virtual void setTimelinePos(qreal p) { this->setPos(p,0.0); }
	virtual void setTimelineScale(qreal scale) { this->rTimelineScale=scale; this->updateBoundingRect(); }
	virtual void setMeasureSpacing(qreal m) { this->rMeasureSpacing=m; }
	virtual void setTopSpacing(qreal s) { this->rTopSpacing=s; }
	virtual void setHeight(qreal h) { this->rHeight=h; this->updateBoundingRect(); }

	Beat timelineBeat() { return this->beatPosition; }
	qreal timelinePos() { return this->pos().x(); }
	qreal measureSpacing() { return this->rMeasureSpacing; }
	qreal topSpacing() { return this->rTopSpacing; }

signals:
	void moved(Beat&);

protected:
	void paint(QPainter*,const QStyleOptionGraphicsItem*,QWidget*){}

	virtual void updateBoundingRect() { this->rectBounds=QRectF(QPointF(0.0,this->rTopSpacing),QSizeF((this->rLength*this->rTimelineScale)-1.0,(this->rHeight-this->rTopSpacing)-1.0)); }

	QRectF rectBounds;

	qreal rTimelineScale = 1.0;
	qreal rTopSpacing = 8.0;
	qreal rMeasureSpacing = 0.0;
	qreal rLength = 0.0;
	qreal rHeight = 0.0;

private:
	Beat beatPosition;
};

#endif // TIMELINEITEM_H
