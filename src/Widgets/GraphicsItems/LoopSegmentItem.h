#ifndef LOOPSEGMENTITEM_H
#define LOOPSEGMENTITEM_H


#include <QGraphicsItem>
#include <QPainter>
#include <QSettings>

#include "PreferencesDialog.h"
#include "TimelineItem.h"
#include "Classes/ClipContainer.h"


class LoopSegmentItem : public TimelineItem
{
public:
	LoopSegmentItem(qreal measurespacing, qreal topspacing) : TimelineItem(measurespacing, topspacing) { this->setZValue(15.0); }

	void setClip(ClipContainerPtr clip) { this->ccClip = clip; this->updateLoop(); }

	void updateLoop()
	{
		this->rLength = -this->ccClip->loopSegment().length.toTimelinePosition(this->rMeasureSpacing, this->ccClip->beatsPerMeasure(), this->ccClip->beatUnit());
		this->updateBoundingRect();
		this->setTimelinePos(this->ccClip->loopSegment().position.toTimelinePosition(this->rMeasureSpacing, this->ccClip->beatsPerMeasure(), this->ccClip->beatUnit()));
	}

protected:
	void paint(QPainter*,const QStyleOptionGraphicsItem*,QWidget*) override;

	virtual void updateBoundingRect() override { this->rectBounds=QRectF(QPointF((this->rLength*this->rTimelineScale),-this->rTopSpacing),QSizeF((-this->rLength*this->rTimelineScale),(TLI_MAX_HEIGHT+this->rTopSpacing))); }

	ClipContainerPtr ccClip = Q_NULLPTR;
};

#endif // LOOPSEGMENTITEM_H
