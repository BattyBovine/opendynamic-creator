#include "LoopSegmentItem.h"


void LoopSegmentItem::paint(QPainter *p, const QStyleOptionGraphicsItem*, QWidget*)
{
	QSettings settings;
	const QColor &colour = settings.value(KEY_LOOP_MARKER_COLOUR).value<QColor>();
	p->setBrush(QBrush(colour));
	p->setPen(colour);

	const qreal topspacing = this->topSpacing();
	const qreal inpos = this->rLength * this->rTimelineScale;

	// Draw marker for loop start position
	QVector<QPointF> incap;
	incap << QPointF(inpos, 0.0)
		  << QPointF(inpos, topspacing)
		  << QPointF(inpos+topspacing, (topspacing/2.0));
	p->drawPolygon(QPolygonF(incap));

	// Draw line for loop start position
	p->drawLine(QPointF(inpos, topspacing), QPointF(inpos, TLI_MAX_HEIGHT));

	// Draw marker for loop end position
	QVector<QPointF> outcap;
	outcap << QPointF(0.0, 0.0)
		   << QPointF(0.0, topspacing)
		   << QPointF(-topspacing, (topspacing/2.0));
	p->drawPolygon(QPolygonF(outcap));

	// Draw line for loop end position
	p->drawLine(QPointF(0.0, topspacing), QPointF(0.0, TLI_MAX_HEIGHT));

	// Draw line between start and end for visual clarity
	p->drawLine(QPointF(inpos,(topspacing/2.0)), QPointF(0.0,(topspacing/2.0)));
}
