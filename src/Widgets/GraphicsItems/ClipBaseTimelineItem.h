#ifndef CLIPBASETIMELINEITEM_H
#define CLIPBASETIMELINEITEM_H

#include <memory>

#include <QImage>
#include <QFile>
#include <QDir>
#include <QCoreApplication>

#include "TimelineItem.h"
#include "Classes/ClipContainer.h"


class ClipBaseTimelineItem : public TimelineItem
{
	Q_OBJECT
public:
	ClipBaseTimelineItem(qreal measurespacing, qreal topspacing) : TimelineItem(measurespacing, topspacing) { this->setZValue(-10.0); }

	virtual void setTimelinePos(Beat p, qreal s) override { TimelineItem::setTimelinePos(p,s); this->updateBoundingRect(); }
	virtual void setTimelinePos(Beat p, qreal s, quint8 b, quint8 u) override { TimelineItem::setTimelinePos(p,s,b,u); this->updateBoundingRect(); }
	virtual void updateLength(qreal) { this->updateBoundingRect(); }

protected:
	void paint(QPainter*,const QStyleOptionGraphicsItem*,QWidget*) override;
};

#endif // CLIPBASETIMELINEITEM_H
