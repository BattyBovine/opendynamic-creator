#ifndef CLIPWAVEFORMTIMELINEITEM_H
#define CLIPWAVEFORMTIMELINEITEM_H

#include <memory>

#include <QImage>
#include <QFile>
#include <QDir>
#include <QCoreApplication>

#include "TimelineItem.h"
#include "Classes/ClipContainer.h"
#include "Classes/WaveformThread.h"

typedef QMap<unsigned int,QList<QPixmap> > WaveformMap;


class ClipWaveformTimelineItem : public TimelineItem
{
	Q_OBJECT
public:
	ClipTimelineItem(ClipContainerPtr clip, qreal measurespacing, qreal topspacing) : TimelineItem(topspacing) {
		this->ccClip = clip;
		TimelineItem::setTimelinePos(clip->timelineOffset(),measurespacing,clip->beatsPerMeasure(),clip->beatUnit());
		this->updateLength(measurespacing);
		this->setZValue(-10.0);
	}

	using TimelineItem::setTimelinePos;
	virtual void setTimelinePos(Beat p, qreal s) { TimelineItem::setTimelinePos(p,s,this->ccClip->beatsPerMeasure(),this->ccClip->beatUnit()); this->ccClip->setTimelineOffset(this->timelineBeat()); this->updateBoundingRect(); }

	void updateLength(qreal s) { this->rLength=this->ccClip->beats().toTimelinePosition(s,this->ccClip->beatsPerMeasure(),this->ccClip->beatUnit()); this->updateBoundingRect(); }
	void setHeight(qreal h) { this->rHeight=h; this->updateBoundingRect(); }
	void setTimelineScale(qreal scale) { this->rTimelineScale=scale; this->generateWaveform(); this->updateBoundingRect(); }

	void generateWaveform();

private slots:
	void getWaveformTile(quint32,int,QPixmap);
	void threadFinished();

protected:
	virtual QRectF boundingRect() const { return this->rectBounds; }
	void paint(QPainter*,const QStyleOptionGraphicsItem*,QWidget*);

private:
	void updateBoundingRect() { this->rectBounds = QRectF(QPointF(0.0,this->topSpacing()),QSizeF((this->rLength*this->rTimelineScale)-1,(this->rHeight-this->topSpacing())-1)); }

	ClipContainerPtr ccClip;
	QRectF rectBounds;
	qreal rLength = 0.0;
	qreal rHeight = 0.0;
	qreal rTimelineScale = 1.0;
	quint8 iWaveformResolution = 0;
	WaveformMap mapWaveforms;
};

#endif // CLIPWAVEFORMTIMELINEITEM_H
