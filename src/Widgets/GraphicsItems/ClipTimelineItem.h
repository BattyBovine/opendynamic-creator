#ifndef CLIPTIMELINEITEM_H
#define CLIPTIMELINEITEM_H

#include <memory>

#include <QImage>
#include <QFile>
#include <QDir>
#include <QCoreApplication>

#include "ClipBaseTimelineItem.h"
#include "Classes/ClipContainer.h"
#include "Classes/WaveformThread.h"

typedef QMap<unsigned int,QList<QPixmap> > WaveformMap;


class ClipTimelineItem : public ClipBaseTimelineItem
{
	Q_OBJECT

public:
	ClipTimelineItem(AudioClipContainerPtr clip, qreal measurespacing, qreal topspacing) : ClipBaseTimelineItem(measurespacing, topspacing)
	{
		this->ccClip = clip;
		TimelineItem::setTimelinePos(clip->timelineOffset(),measurespacing,clip->beatsPerMeasure(),clip->beatUnit());
		this->updateLength(measurespacing);
	}

	virtual void setTimelinePos(Beat p, qreal s) override { TimelineItem::setTimelinePos(p,s,this->ccClip->beatsPerMeasure(),this->ccClip->beatUnit()); this->ccClip->setTimelineOffset(this->timelineBeat()); this->updateBoundingRect(); }

	void setTimelineScale(qreal scale) override { ClipBaseTimelineItem::setTimelineScale(scale); this->generateWaveform(); }
	void updateLength(qreal s) override { this->rLength=this->ccClip->beats().toTimelinePosition(s,this->ccClip->beatsPerMeasure(),this->ccClip->beatUnit()); this->updateBoundingRect(); }

	void generateWaveform();

private slots:
	void getWaveformTile(quint32,int,QPixmap);
	void threadFinished();

protected:
	void paint(QPainter*,const QStyleOptionGraphicsItem*,QWidget*) override;

private:
	AudioClipContainerPtr ccClip;
	quint8 iWaveformResolution = 0;
	WaveformMap mapWaveforms;
};

#endif // CLIPTIMELINEITEM_H
