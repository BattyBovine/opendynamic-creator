#ifndef PLAYMARKERITEM_H
#define PLAYMARKERITEM_H

#include <QGraphicsItem>
#include <QPainter>
#include <QSettings>

#include "TimelineItem.h"
#include "PreferencesDialog.h"


class PlayMarkerItem : public TimelineItem
{
public:
	PlayMarkerItem(qreal measurespacing, qreal topspacing) : TimelineItem(measurespacing, topspacing) { this->setZValue(20.0); }

protected:
	void paint(QPainter*,const QStyleOptionGraphicsItem*,QWidget*);
};

#endif // PLAYMARKERITEM_H
