#ifndef MUSICPROPERTIESWIDGET_H
#define MUSICPROPERTIESWIDGET_H

#include <QScrollArea>

#include "Widgets/MusicTreeView.h"

namespace Ui {
class MusicPropertiesWidget;
}

class MusicPropertiesWidget : public QScrollArea
{
	Q_OBJECT

public:
	explicit MusicPropertiesWidget(TrackItem *track, QWidget *parent = Q_NULLPTR);
	~MusicPropertiesWidget();

	void attachTrackItem(TrackItem*);

	int tempo();
	int beatsPerMeasure();
	int beatUnit();
	double playbackSpeed();

public slots:
	void tempoChanged(qreal t);
	void setTempo(qreal t);
	void beatsPerMeasureChanged(int b);
	void setBeatsPerMeasure(int b);
	void beatUnitChanged(int b);
	void setBeatUnit(int b);
	void playbackSpeedChanged(qreal s);
	void setPlaybackSpeed(qreal s);

private:
	Ui::MusicPropertiesWidget *ui = Q_NULLPTR;

	TrackItem *tiTrack = Q_NULLPTR;
};

#endif // MUSICPROPERTIESWIDGET_H
