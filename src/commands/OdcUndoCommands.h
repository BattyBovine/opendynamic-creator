#ifndef ODCUNDOCOMMANDS_H
#define ODCUNDOCOMMANDS_H

#include <QUndoCommand>

#include "Widgets/MusicTreeView.h"
#include "Widgets/EventTreeView.h"

class OdcAddItemCommand : public QUndoCommand
{
public:
    OdcAddItemCommand(QStandardItem*, QStandardItem*, QTreeView*, QStandardItemModel*, QItemSelectionModel*, QUndoCommand *parent=Q_NULLPTR);
    void undo();
    void redo();
private:
	QStandardItem *item = Q_NULLPTR;
	QStandardItem *parent = Q_NULLPTR;
	QTreeView *view = Q_NULLPTR;
	QStandardItemModel *model = Q_NULLPTR;
	QItemSelectionModel *selmodel = Q_NULLPTR;
    int row;
};



class OdcAddStateSwitchCommand : public QUndoCommand
{
public:
	OdcAddStateSwitchCommand(EventItem*, QTreeView*, QStandardItemModel*, QItemSelectionModel*, QUndoCommand *parent=Q_NULLPTR);
	void undo();
	void redo();
private:
	QStandardItem *stateswitch = Q_NULLPTR;
	QTreeView *view = Q_NULLPTR;
	QStandardItemModel *model = Q_NULLPTR;
	QItemSelectionModel *selmodel = Q_NULLPTR;
	int row;
};

#endif // ODCUNDOCOMMANDS_H
