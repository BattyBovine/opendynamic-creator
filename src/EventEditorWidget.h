#ifndef EVENTEDITORWIDGET_H
#define EVENTEDITORWIDGET_H

#include <QWidget>

namespace Ui {
class EventEditorWidget;
}

class EventEditorWidget : public QWidget
{
	Q_OBJECT

public:
	explicit EventEditorWidget(QWidget *parent = Q_NULLPTR);
	~EventEditorWidget();

private:
	Ui::EventEditorWidget *ui = Q_NULLPTR;
};

#endif // EVENTEDITORWIDGET_H
